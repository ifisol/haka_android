package haka.ifisol.haka.Utilities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

import haka.ifisol.haka.R;


public class Utility {

    public static boolean isConnected(Context context) {

        boolean available = false;
       // try {
            if (context != null) {
                final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                if (activeNetwork != null && activeNetwork.getState() == NetworkInfo.State.CONNECTED) {
                    available = true;

                }
            }else {
                    available = false;
                    Toast.makeText(context, "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                }

       // } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
           /// Toast.makeText(context, "Internet Connection Lost", Toast.LENGTH_SHORT).show();
       // }
        return available;
    }
//Open Activity

    public static void OpenActivityPrevious(Context context, Activity activity, Class<?> cls) {

        Intent mIntent = new Intent(context, cls);
        context.startActivity(mIntent);
        activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    public static void OpenActivityNext(Context context, Activity activity, Class<?> cls) {

        Intent mIntent = new Intent(context, cls);
        context.startActivity(mIntent);
        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public static void OpenActivityNextByData(Intent mIntent, Activity activity) {

        activity.startActivity(mIntent);
        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public static void OpenActivityPreviousByData(Intent mIntent, Activity activity) {

        activity.startActivity(mIntent);
        activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }
    //replace fragments

   public static void ReplaceFragment(Fragment fragment, FragmentManager manager,String fragName) {

        FragmentManager fragmentManager = manager;
        fragmentManager.beginTransaction().replace(R.id.fram_Container, fragment).addToBackStack(fragName).commit();
    }

    public static void ReplaceFragment(Fragment fragment, FragmentManager manager, Bundle b) {

        fragment.setArguments(b);
        FragmentManager fragmentManager = manager;
        fragmentManager.beginTransaction().replace(R.id.fram_Container, fragment).addToBackStack("").commit();
    }
    public static void ReplaceFragmentYoutub(Fragment fragment, FragmentManager manager, Bundle b) {

        fragment.setArguments(b);
        FragmentManager fragmentManager = manager;
        fragmentManager.beginTransaction().replace(R.id.fram_Container_youtube, fragment).addToBackStack("").commit();
    }

    public static String GetDeviceToken(Context mContext) {

        return Settings.Secure.getString(mContext.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String GetMonth(int m) {

        String month = "";
        switch (m) {
            case 1:
                month = "Jan";
                break;
            case 2:
                month = "Feb";
                break;
            case 3:
                month = "Mar";
                break;
            case 4:
                month = "Apr";
                break;
            case 5:
                month = "May";
                break;
            case 6:
                month = "Jun";
                break;
            case 7:
                month = "Jul";
                break;
            case 8:
                month = "Aug";
                break;
            case 9:
                month = "Sep";
                break;
            case 10:
                month = "Oct";
                break;
            case 11:
                month = "Nov";
                break;
            case 12:
                month = "Dec";
                break;
        }
        return month;
    }

    public static boolean EmailValidator(String email) {

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    /*public static boolean isGooglePlayServicesAvailable(Context context) {

        boolean result;
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode == ConnectionResult.SUCCESS) result = true;
        else result = false;
        return result;
    }*/

    public static String EncodeTobase64(Bitmap image) {

        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 50, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }
    public static void ShowAlertDialog(Activity mActivity, String title, String msg) {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView header = (TextView) dialog.findViewById(R.id.txt_header);
        TextView message = (TextView) dialog.findViewById(R.id.txt_msg);
        header.setText(title);
        message.setText(msg);
        Button btnOK = (Button) dialog.findViewById(R.id.btn_ok);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });

        dialog.show();
    }
    public static boolean isPackageExisted(Context c, String targetPackage) {

        PackageManager pm = c.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage,
                    PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }


}
