package haka.ifisol.haka.Controller.Async;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.View.Activities.DeshboardActvty;


/**
 * Created by MEH on 1/9/2017.
 */
public class ChangePasswordTask extends AsyncTask<String ,Void,String> {
    DeshboardActvty mActivity;
    private String resp;
    haka.ifisol.haka.Controller.Async.JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;

    public ChangePasswordTask(DeshboardActvty activity, HashMap<String, String> params) {
        mActivity = activity;
        jsonArrayParser = new JSONArrayParser();
        mActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameters = params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.CHANG_PASSWORD);
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {

                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    //mActivity.API_RESULT();

                } else
                {
                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
