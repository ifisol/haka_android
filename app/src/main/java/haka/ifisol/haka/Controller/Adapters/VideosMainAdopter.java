package haka.ifisol.haka.Controller.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import haka.ifisol.haka.View.Fragments.DailymotionRecyclerfragment;
import haka.ifisol.haka.View.Fragments.DownloadFragment;
import haka.ifisol.haka.View.Fragments.YoutubeVideosFragment;

/**
 * Created by MEH on 1/27/2017.
 */

public class VideosMainAdopter extends FragmentPagerAdapter {
    ArrayList<Fragment> mFragList;
    public VideosMainAdopter(FragmentManager fragmentManager){
        super(fragmentManager);
        mFragList=new ArrayList<>();
        mFragList.add(new DailymotionRecyclerfragment());
        mFragList.add(new YoutubeVideosFragment());
       // mFragList.add(new DownloadFragment());
    }
    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        switch (position){
            case 0:
                fragment= mFragList.get(0);
                ;
                break;
            case 1:
                fragment= mFragList.get(1);
                break;
         /*   case 2:
                fragment= mFragList.get(2);
                break;*/
        }
        return fragment;
    }
    @Override
    public int getCount() {
        return 2;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        String title=" ";
        switch (position){
            case 0:
                title="Dailymotion";
                break;
            case 1:
                title="Youtube";
                break;
       /*     case 2:
                title="Downlowds";
                break;*/
        }

        return title;
    }

}
