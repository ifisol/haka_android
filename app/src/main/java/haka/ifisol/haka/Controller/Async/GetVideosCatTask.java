package haka.ifisol.haka.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Async.JSONArrayParser;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.Model.VideoAcat;
import haka.ifisol.haka.Model.VideoSubCat;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Fragments.SearchFragment;

/**
 * Created by MEH on 1/11/2017.
 */
public class GetVideosCatTask extends AsyncTask<String,Void,String> {
    SearchFragment searchFragment;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    Context mContext;
    DeshboardActvty actvty;
    ArrayList<VideoAcat> mArray;
    ArrayList<VideoSubCat>mAray;
    HashMap<String, String> parameters;
    public GetVideosCatTask(Context context, SearchFragment frag, DeshboardActvty actvty, HashMap<String, String> param) {

        parameters = param;
        mContext=context;
        searchFragment=frag;
        this.actvty=actvty;
        mArray=new ArrayList<>();
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        actvty.mViewProgressBar.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.GRT_VIDEOS_Cat);
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        actvty.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {

                    JSONArray videoArray=jsonObject.getJSONArray("response");
                    for (int i=0;i<videoArray.length();i++)
                    {

                        JSONObject obj=videoArray.getJSONObject(i);
                        VideoAcat videocat=new VideoAcat();
                        videocat.setVideo_cat_id(obj.getString("vc_id"));
                        videocat.setVideo_cat_name(obj.getString("vc_cat_name"));
                        videocat.setVideo_Cat_keywords(obj.getString("vc_cat_keywords"));
                        videocat.setVideo_image(obj.getString("video_image"));
                        JSONArray subcatArray=obj.getJSONArray("subcat");
                        mAray=new ArrayList<>();

                        for (int j=0;j<subcatArray.length();j++)
                        {

                            JSONObject subcat_obj=subcatArray.getJSONObject(j);
                            VideoSubCat videoSubCat=new VideoSubCat();
                            videoSubCat.setVideo_sub_cat_id(subcat_obj.getString("vsc_id"));
                            videoSubCat.setVideo_sub_cat_name(subcat_obj.getString("vsc_sub_cat_name"));
                            videoSubCat.setVideo_sub_cat_keyword(subcat_obj.getString("vsc_sub_cat_keywords"));
                            mAray.add(videoSubCat);
                        }
                        videocat.setArrayList(mAray);
                        mArray.add(videocat);
                    }
                    searchFragment.API_RESULT(mArray);

                } else
                {
                    Toast.makeText(actvty, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(actvty, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
