package haka.ifisol.haka.Controller.Async;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.Model.DailyMotion;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Fragments.DailymotionRecyclerfragment;


/**
 * Created by MEH on 1/10/2017.
 */
public class GetDailyMotionVideosTask extends AsyncTask<String ,Void,String> {

    DeshboardActvty mActivity;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;
    DailymotionRecyclerfragment dailyMotionFragment;
    String searchthrough;
   /* DailyMotionFragment dailyMotionFragment;*/

    public GetDailyMotionVideosTask(DailymotionRecyclerfragment frag, DeshboardActvty mMenueActivity, HashMap<String, String> param,String searchThrough) {
        mActivity = mMenueActivity;
        parameters = param;
        searchthrough=searchThrough;
        jsonArrayParser = new JSONArrayParser();
        dailyMotionFragment = frag;
        if (searchThrough.equalsIgnoreCase("referesh"))
        {
            mActivity.mViewProgressBar.setVisibility(View.GONE);
        }
        else
        mActivity.mViewProgressBar.setVisibility(View.VISIBLE);

    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl(parameters, Constants.DailyMotionBaseUrl);

        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        DailyMotion videos;
        ArrayList<DailyMotion> arrayList = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty() || result != null) {
                jsonObject = new JSONObject(result);

                JSONArray jsonArray = jsonObject.getJSONArray("list");
                for (int i = 0; i < jsonArray.length(); i++) {
                    videos=new DailyMotion();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    videos.setVideoID(jsonObject1.getString("id"));
                    videos.setVideoURL(jsonObject1.getString("thumbnail_url"));
                    videos.setVideoTitle(jsonObject1.getString("title"));
                    arrayList.add(videos);

                }

                dailyMotionFragment.API_RESULT(arrayList);


            } else {
                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}
