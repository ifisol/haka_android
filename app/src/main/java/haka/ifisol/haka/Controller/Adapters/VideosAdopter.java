package haka.ifisol.haka.Controller.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import haka.ifisol.haka.R;
import haka.ifisol.haka.Model.News;
import haka.ifisol.haka.Model.Videos;
import haka.ifisol.haka.Model.Youtube;

/**
 * Created by MEH on 12/23/2016.
 */
public class VideosAdopter extends RecyclerView.Adapter<VideosAdopter.MyViewHolder> {
    private static ClickListener clickListener;
    Context mContext;
    ImageView imgThumbnail;
    ImageView imgPlay;
    TextView tvVideoTitle;
   // TextView tv_Download;
    ImageView imgDownload;
    public ArrayList<Youtube>arrayList=new ArrayList<>();
    public VideosAdopter(ArrayList<Youtube> arr, Context con)
    {
        arrayList=arr;
        mContext=con;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public MyViewHolder(View itemView) {
            super(itemView);
            imgPlay=(ImageView)itemView.findViewById(R.id.img_play);
            tvVideoTitle=(TextView)itemView.findViewById(R.id.tv_dm_video_name);
            imgThumbnail=(ImageView)itemView.findViewById(R.id.img_dm_videos);
            //tv_Download=(TextView)itemView.findViewById(R.id.tv_downloads);
            imgDownload=(ImageView) itemView.findViewById(R.id.img_dnload);
            imgPlay.setOnClickListener(this);
            //tv_Download.setOnClickListener(this);
            imgDownload.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
               clickListener.onItemClick(getPosition(),v);
        }
    }
    @Override
    public  VideosAdopter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_youtub_videos, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Youtube youtube=arrayList.get(position);
        tvVideoTitle.setText(youtube.getVideo_Ytitle());
        String userimg=youtube.getVideo_Ythunbail();
        if (userimg!=null && userimg.length()>0) {
            Picasso.with(mContext)
                    .load(userimg)
                    //.error(R.drawable.capture)
                    .into(imgThumbnail);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public interface ClickListener {
        void onItemClick(int position, View v);

    }
    public void setOnItemClickListener(ClickListener clickListener) {
        VideosAdopter.clickListener = clickListener;
    }
}
