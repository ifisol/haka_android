package haka.ifisol.haka.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Fragments.DownloadFragment;
import haka.ifisol.haka.View.Fragments.DownloadedVideosRecyclerFrag;
import haka.ifisol.haka.View.Fragments.Fragment_new_setting;
import haka.ifisol.haka.View.Fragments.SettingsFragment;

import static android.R.id.tabs;

/**
 * Created by MEH on 1/30/2017.
 */

public class SettingsAdopter extends FragmentPagerAdapter {
    ArrayList<Fragment> mFragList;
    Context mContxt;
    ArrayList<Integer>tabIcons;
    public SettingsAdopter(FragmentManager fragmentManager, Context context){
        super(fragmentManager);
        mFragList=new ArrayList<>();
        tabIcons=new ArrayList<>();
        mContxt=context;
        mFragList.add(new DownloadedVideosRecyclerFrag());
        mFragList.add(new DownloadedVideosRecyclerFrag());
        mFragList.add(new SettingsFragment());
        tabIcons.add(R.drawable.ic_action_grid);
        tabIcons.add(R.drawable.ic_action_list);
        tabIcons.add(R.drawable.ic_action_user_outline);


    }
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = mFragList.get(position);
        if (position==0||position==1)
        {
            Bundle bundle=new Bundle();
            bundle.putInt("POSITION",position);
            fragment.setArguments(bundle);

        }
        return fragment;
    }
    @Override
    public int getCount() {
        return 3;
    }
   public View getTabView(int position) {
        View v = LayoutInflater.from(mContxt).inflate(R.layout.custom_tab_layout, null);
        ImageView img = (ImageView) v.findViewById(R.id.img_tab);
       img.setImageResource(tabIcons.get(position));
       return v;
    }
    /* @Override
    public CharSequence getPageTitle(int position) {
        String title=" ";
        switch (position){
            case 0:
                title="Dailymotion";
                break;
            case 1:
                title="Youtube";
                break;
            case 2:
                title="Downlowds";
                break;
        }

        return title;
    }*/
}
