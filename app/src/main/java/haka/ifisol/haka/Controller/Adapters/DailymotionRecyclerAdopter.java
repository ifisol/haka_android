package haka.ifisol.haka.Controller.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import haka.ifisol.haka.Model.DailyMotion;
import haka.ifisol.haka.R;

/**
 * Created by MEH on 1/27/2017.
 */

public class DailymotionRecyclerAdopter extends RecyclerView.Adapter<DailymotionRecyclerAdopter.MyViewHolder> {
    private static ClickListener clickListenerrr;
    Context mContext;
    ImageView imgThumbnail;
    ImageView imgPlayy;
    TextView tvVideoTitle;
    // TextView tv_Download;
    ImageView imgDownload;
    ArrayList<DailyMotion> mArray=new ArrayList<DailyMotion>();
    public DailymotionRecyclerAdopter(ArrayList<DailyMotion> arrayList, Context context)
    {
        mArray=arrayList;
        this.mContext=context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public MyViewHolder(View itemView) {
            super(itemView);
            imgPlayy=(ImageView)itemView.findViewById(R.id.img_play_dailymotion);
            tvVideoTitle=(TextView)itemView.findViewById(R.id.tv_video_title);
            imgThumbnail=(ImageView)itemView.findViewById(R.id.img_dm_videos);
            //tv_Download=(TextView)itemView.findViewById(R.id.tv_downloads);
            //imgDownload=(ImageView) itemView.findViewById(R.id.img_dnload);
            imgPlayy.setOnClickListener(this);
            //tv_Download.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListenerrr.onItemClick(getPosition(),v);
        }
    }
    @Override
      public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_videos, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DailyMotion dailyMotion=mArray.get(position);
        tvVideoTitle.setText(dailyMotion.getVideoTitle());
        String userimg=dailyMotion.getVideoURL();
        if (userimg!=null && userimg.length()>0) {
            Picasso.with(mContext)
                    .load(userimg)
                    //.error(R.drawable.capture)
                    .into(imgThumbnail);
        }

    }

    @Override
    public int getItemCount() {
        return mArray.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public interface ClickListener {
        void onItemClick(int position, View v);

    }
    public void setOnItemClickListener(ClickListener clickListener) {
        DailymotionRecyclerAdopter.clickListenerrr = clickListener;
    }
}
