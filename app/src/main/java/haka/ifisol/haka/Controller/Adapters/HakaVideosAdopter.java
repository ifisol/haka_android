package haka.ifisol.haka.Controller.Adapters;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.DownloadedVideos;
import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Fragments.DownloadFragment;


/**
 * Created by MEH on 12/29/2016.
 */
public class HakaVideosAdopter extends BaseAdapter {
    private Context context;
    public ArrayList<String> array;
    GlobalActivity globalActivity;
    DownloadFragment donloadfrag;
    ArrayList<DownloadedVideos>videosArray;
    ArrayList<DownloadedVideos>mList=new ArrayList<DownloadedVideos>();
    int DonwnLdposition = -11;
    int dl_progress;
    String pausedReason = "";
    boolean isThreadStarted;
    public HakaVideosAdopter(Context context, ArrayList<DownloadedVideos> fileList, DownloadFragment downloadFragment) {
        this.context = context;
        globalActivity=(GlobalActivity) context.getApplicationContext();
       // videosArray=fileList;
        if (globalActivity.GetDownloadingVideo()!=null)
        {
            for (int i=0;i<fileList.size();i++)
            {
                if (globalActivity.GetDownloadingVideo().getVideoName().equals(fileList.get(i).getVideoName()))
                {
                    mList.add(0,fileList.get(i));
                    fileList.remove(i);
                    mList.addAll(fileList);
                }
            }


        }
        else
        {
            mList=fileList;
        }
        donloadfrag=downloadFragment;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.gridlayout_video, null);
        convertView.setTag(new ViewHolder(convertView));
    }
       // File videoFiles = new File(Environment.getExternalStorageDirectory().toString()+"/Haka");
        //String url=videoFiles+""+"/"+VideoValues[position];
        //String url=videoFiles+""+"/"+array.get(position);
        initializeViews((ViewHolder) convertView.getTag(),position);
        return convertView;
     }

    private void initializeViews(final ViewHolder h, int position) {



        if (mList.get(position).isDownloading)
        {
            if (mList.get(position).getPosition()>=100)
            {
                mList.get(position).setDownloading(false);
                mList.get(position).setPosition(100);
                h.progressBar.setVisibility(View.GONE);
                h.tv_percentag.setVisibility(View.GONE);
                h.tvStatus.setVisibility(View.GONE);

            }
            else
            {
                h.tvStatus.setVisibility(View.VISIBLE);
                h.progressBar.setVisibility(View.VISIBLE);
                h.progressBar.setProgress(mList.get(position).getPosition());
                h.tv_percentag.setVisibility(View.VISIBLE);
                h.tv_percentag.setText(mList.get(position).getPosition()+"%");
                h.tvStatus.setText(pausedReason);
            }

        }
        else
        {
            h.tvStatus.setVisibility(View.GONE);
            h.progressBar.setVisibility(View.GONE);
            h.tv_percentag.setVisibility(View.GONE);
        }

        if (globalActivity.GetDownloadingVideo()!=null)
        {
            if (!isThreadStarted)
            {
                for (DownloadedVideos obj: mList)
                {
                    if (obj.getVideoName().equals(globalActivity.GetDownloadingVideo().getVideoName()))
                        DonwnLdposition = mList.indexOf(obj);

                }
                if (DonwnLdposition!=-11)
                {

                    final DownloadManager manager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
                    new Thread(new Runnable() {

                        @Override
                        public void run() {

                            boolean downloading = true;
                            isThreadStarted=true;

                            while (downloading) {

                                    DownloadManager.Query q = new DownloadManager.Query();
                                    q.setFilterById(globalActivity.GetDownloadingVideo().getVideoID());
                                    Cursor cursor = manager.query(q);

                                    if (cursor.moveToFirst()) {
                                        final int bytes_downloaded = cursor.getInt(cursor
                                                .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                                        int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                                        int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                                        int status = cursor.getInt(columnIndex);
                                        int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
                                        int reason = cursor.getInt(columnReason);
                                        switch (status) {
                                            case DownloadManager.STATUS_PAUSED:
                                                //h.tvStatus.setVisibility(View.VISIBLE);
                                                switch (reason) {
                                                    case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
                                                        pausedReason = "PAUSED QUEUED FOR WIFI";
                                                        break;
                                                    case DownloadManager.PAUSED_UNKNOWN:
                                                        pausedReason = "PAUSED UNKNOWN";
                                                        break;
                                                    case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
                                                        pausedReason = "PAUSED WAITING FOR NETWORK";
                                                        break;
                                                    case DownloadManager.PAUSED_WAITING_TO_RETRY:
                                                        pausedReason = "PAUSED WAITING TO RETRY";
                                                        break;
                                                }
                                                break;
                                            case DownloadManager.STATUS_SUCCESSFUL:
                                                downloading = false;
                                                pausedReason = "";
                                                break;
                                            case DownloadManager.STATUS_RUNNING:
                                                pausedReason = "DOWNLOADING...";
                                                // h.tv_percentag.setVisibility(View.VISIBLE);
                                                // h.tvStatus.setVisibility(View.GONE);
                                                break;
                                        }
                                        dl_progress = (int) ((double) bytes_downloaded / (double) bytes_total * 100f);

                                        mList.get(DonwnLdposition).setDownloading(true);
                                        mList.get(DonwnLdposition).setPosition(dl_progress);

                                    }
                               /* if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                                    downloading = false;
                                }*/



                            /*    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.PAUSED_WAITING_FOR_NETWORK) {

                                    downloading = false;

                                }*/



                                    try {
                                        Thread.sleep(3000);
                                        donloadfrag.getActivity().runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {

                                                notifyDataSetChanged();
                                                if (dl_progress>=100)
                                                {
                                                    isThreadStarted=false;
                                                    Thread.currentThread().interrupt();
                                                    globalActivity.SaveDownloadingVideo(null);

                                                }


                                            }

                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    cursor.close();

                            }

                        }
                    }).start();

                }
            }
            }


        h.textView.setText(mList.get(position).getVideoName());
        h.imageThumbnail.setImageBitmap(mList.get(position).getVideoThunmbNail());
        h.tVtime.setText(mList.get(position).getVideoDate());

       /* MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(context, Uri.parse(url));
        String time1 = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DATE);
        String title=  retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(url, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);*/


    }

    protected class ViewHolder {
        TextView textView;
        TextView tVtime;
        ImageView imageThumbnail;
        ProgressBar progressBar;
        TextView tv_percentag;
        TextView tvStatus;


       public ViewHolder(View view) {
           tvStatus = (TextView)view.findViewById(R.id.tv_status);
           textView = (TextView)view.findViewById(R.id.grid_item_label);
           tVtime = (TextView)view.findViewById(R.id.tv_date);
           imageThumbnail = (ImageView)view.findViewById(R.id.grid_item_image);
           progressBar=(ProgressBar)view.findViewById(R.id.progressbar);
           tv_percentag=(TextView)view.findViewById(R.id.tv_percent);
    }
}

    public void RemoveItem(int pos)
    {
       // videosArray.remove(videosArray.get(pos).getVideoName());
        mList.remove(pos);
        Thread.currentThread().interrupt();
        isThreadStarted=false;
        notifyDataSetChanged();

    }
}
