package haka.ifisol.haka.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Async.JSONArrayParser;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.Model.News;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Fragments.NewsFilterFragment;

/**
 * Created by MEH on 1/4/2017.
 */
public class GetNewsTask extends AsyncTask<String,Void,String> {
    NewsFilterFragment newsFilterFragment;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    Context mContext;
    DeshboardActvty actvty;
    ArrayList<News> mArray;
    HashMap<String, String> parameters;
    public GetNewsTask(Context context, NewsFilterFragment frag, DeshboardActvty actvty, HashMap<String, String> param) {

        parameters = param;
        mContext=context;
        newsFilterFragment=frag;
        this.actvty=actvty;
        mArray=new ArrayList<>();
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        actvty.mViewProgressBar.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.GET_NEWS);
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        actvty.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {

                    JSONArray reponse=jsonObject.getJSONArray("response");
                    for (int i=0;i<reponse.length();i++)
                    {
                        JSONObject obj=reponse.getJSONObject(i);
                        News news=new News();
                        news.setNews_id(obj.getString("nc_id"));
                        news.setNews_name(obj.getString("nc_cat_name"));
                        news.setNews_keyword(obj.getString("nc_cat_keywords"));
                        mArray.add(news);
                    }


                    newsFilterFragment.API_RESULT(mArray);

                } else
                {
                    Toast.makeText(actvty, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(actvty, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
