package haka.ifisol.haka.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.Model.Events;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Fragments.EventsFilterFragment;


/**
 * Created by MEH on 1/3/2017.
 */
public class GetEventTask extends AsyncTask<String,Void,String> {
    EventsFilterFragment eventsFilterFragment;
    private String resp;
    GlobalActivity mGlobal;
    haka.ifisol.haka.Controller.Async.JSONArrayParser jsonArrayParser;
    Context mContext;
    DeshboardActvty actvty;
    ArrayList<Events>mArray;
    HashMap<String, String> parameters;
    public GetEventTask(Context context, EventsFilterFragment frag, DeshboardActvty actvty, HashMap<String, String> param) {

        parameters = param;
        mContext=context;
        eventsFilterFragment=frag;
        this.actvty=actvty;
        mArray=new ArrayList<>();
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
        jsonArrayParser = new haka.ifisol.haka.Controller.Async.JSONArrayParser();
        actvty.mViewProgressBar.setVisibility(View.VISIBLE);

}

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.GET_EVENT);
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        actvty.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {

                    JSONArray reponse=jsonObject.getJSONArray("response");
                    for (int i=0;i<reponse.length();i++)
                    {
                        JSONObject obj=reponse.getJSONObject(i);
                        Events events=new Events();
                        events.setCat_id(obj.getString("es_id"));
                        events.setCat_name(obj.getString("es_cat_name"));
                        events.setCat_keyword(obj.getString("es_cat_keywords"));
                        mArray.add(events);
                    }


                    eventsFilterFragment.API_RESULT(mArray);

                } else
                {
                    Toast.makeText(actvty, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(actvty, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
