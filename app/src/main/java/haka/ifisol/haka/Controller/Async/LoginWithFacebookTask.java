package haka.ifisol.haka.Controller.Async;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.Model.User;
import haka.ifisol.haka.View.Activities.RegisterWithFacebookActivity;

/**
 * Created by MEH on 1/9/2017.
 */
public class LoginWithFacebookTask extends AsyncTask<String,Void,String> {
    RegisterWithFacebookActivity mActivity;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;

    public LoginWithFacebookTask(RegisterWithFacebookActivity activity, HashMap<String, String> params) {
        mActivity = activity;
        mGlobal=(GlobalActivity)mActivity.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();
        mActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameters = params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.LOGIN);
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {

                    JSONObject reponse=jsonObject.getJSONObject("response");
                    User user=new User();
                    user.setUser_name(reponse.getString("user_name"));
                    user.setUser_id(reponse.getString("user_id"));
                    user.setUser_email(reponse.getString("email"));
                    user.setFacebookUser(reponse.getString("is_facebook_user"));
                    user.setUser_last_login_date(reponse.getString("last_login_date"));
                    String photo=reponse.getString("photo");
                    if (!photo.equals("")|| !(photo ==null))
                    {
                        user.setPicture(reponse.getString("photo"));
                    }
                    user.setUser_registeration_date(reponse.getString("registration_date"));
                    mGlobal.SaveActiveUser(user);
                    mActivity.API_RESULT();

                } else
                {
                    mActivity.LOGIN_RESULT();
                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
