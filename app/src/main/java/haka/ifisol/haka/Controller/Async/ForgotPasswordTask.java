package haka.ifisol.haka.Controller.Async;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import haka.ifisol.haka.Controller.Async.JSONArrayParser;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.View.Activities.LoginActivity;


/**
 * Created by MEH on 1/3/2017.
 */
public class ForgotPasswordTask extends AsyncTask<String,Void,String> {
    LoginActivity mActivity;
    private String resp;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;

    public ForgotPasswordTask(LoginActivity activity, HashMap<String, String> params) {
        mActivity = activity;
        jsonArrayParser = new JSONArrayParser();
        mActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        parameters = params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.FORGOT_PASSWORD);
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {

                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                } else
                {
                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
