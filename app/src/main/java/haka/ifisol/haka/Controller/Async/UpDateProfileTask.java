package haka.ifisol.haka.Controller.Async;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.Model.User;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Fragments.SettingsFragment;


/**
 * Created by MEH on 1/9/2017.
 */
public class UpDateProfileTask extends AsyncTask<String,Void,String> {
    DeshboardActvty mActivity;
    private String resp;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;
    SettingsFragment frag;
    GlobalActivity mGlobal;

    public UpDateProfileTask(HashMap<String, String> param, DeshboardActvty mMenueActivity, SettingsFragment fragmenttt) {
        mActivity = mMenueActivity;
        jsonArrayParser = new JSONArrayParser();
        mActivity.mViewProgressBar.setVisibility(View.VISIBLE);
        mGlobal=(GlobalActivity)mActivity.getApplicationContext();
        parameters = param;
        frag=fragmenttt;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl_POST(parameters, Constants.UPDATE_Profile);
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        mActivity.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);
                if (jsonObject.getString("status").equals("true")) {
                    JSONObject reponse=jsonObject.getJSONObject("response");
                    User user=new User();
                    user.setUser_name(reponse.getString("user_name"));
                    user.setUser_id(reponse.getString("user_id"));
                    user.setUser_email(reponse.getString("email"));
                    user.setUser_password(reponse.getString("password"));
                    user.setFacebookUser(reponse.getString("is_facebook_user"));
                    user.setUser_last_login_date(reponse.getString("last_login_date"));
                    String photo=reponse.getString("photo");
                    if (!photo.equals(""))
                    {
                        user.setPicture(reponse.getString("photo"));
                    }
                    user.setUser_registeration_date(reponse.getString("registration_date"));
                    mGlobal.SaveActiveUser(user);
                    frag.API_RESULT();

                } else
                {
                    Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(mActivity, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
