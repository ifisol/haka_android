package haka.ifisol.haka.Controller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import haka.ifisol.haka.R;
import haka.ifisol.haka.Model.DailyMotion;
import haka.ifisol.haka.Model.Youtube;

/**
 * Created by MEH on 1/11/2017.
 */
public class YoutubeAdopter extends BaseAdapter {
    ArrayList<Youtube> mArray=new ArrayList<Youtube>();
    Context context;

    public YoutubeAdopter(ArrayList<Youtube> arrayList, Context context) {
        mArray=arrayList;
        this.context=context;

    }

    @Override
    public int getCount() {
        return mArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(R.layout.item_dailymotion,null);
            TextView tv_vieo_name=(TextView)row.findViewById(R.id.tv_dm_video_name);
            ImageView imgViewThumbnail=(ImageView)row.findViewById(R.id.img_dm_videos);
            Youtube youtube=new Youtube();
            youtube=mArray.get(position);
            tv_vieo_name.setText(youtube.getVideo_Ytitle());
            Picasso.with(context)
                    .load(youtube.getVideo_Ythunbail())
                    .error(R.drawable.capture)
                    .into(imgViewThumbnail);
        return row;
    }
}
