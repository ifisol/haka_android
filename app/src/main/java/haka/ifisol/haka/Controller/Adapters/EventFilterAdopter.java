package haka.ifisol.haka.Controller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import haka.ifisol.haka.Model.Events;
import haka.ifisol.haka.R;


/**
 * Created by MEH on 1/2/2017.
 */
public class EventFilterAdopter extends BaseAdapter {
    Context context;
    int nmber;
    ArrayList<Events> mArray=new ArrayList<>();

    public EventFilterAdopter(Context mContext, ArrayList<Events>array) {
        context=mContext;
        mArray=array;
        nmber=1;
    }

    @Override
    public int getCount() {
        return mArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        if (convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(R.layout.item_filter_event,null);
            TextView tv_cat_name=(TextView)row.findViewById(R.id.tv_cat_name);
            TextView tv_nmber=(TextView)row.findViewById(R.id.tv_number);

            Events events=mArray.get(position);
            tv_cat_name.setText(events.getCat_name());
            tv_nmber.setText(String.valueOf(nmber));
            nmber++;

        }
        else
            row=convertView;



        return row;
    }
}
