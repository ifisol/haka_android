package haka.ifisol.haka.Controller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.Model.VideoAcat;
import haka.ifisol.haka.R;

/**
 * Created by MEH on 12/30/2016.
 */
public class SearchAdopter extends BaseAdapter  {
    Context context;
    public ArrayList<VideoAcat>arrayList=new ArrayList<>();
    public SearchAdopter(Context mContext, ArrayList<VideoAcat>array) {
        context=mContext;
        arrayList=array;

    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(R.layout.item_search,null);
            TextView tv_textview=(TextView)row.findViewById(R.id.tv_name);
        ImageView imgSearchCat=(ImageView) row.findViewById(R.id.img_video_search);
            VideoAcat videoAcat=new VideoAcat();
            videoAcat=arrayList.get(position);
            tv_textview.setText(videoAcat.getVideo_cat_name());
        Picasso.with(context)
                .load(Constants.Img_URL + videoAcat.getVideo_image())
                .error(R.drawable.capture)
                .into(imgSearchCat);

        return row;
    }
}
