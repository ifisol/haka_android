package haka.ifisol.haka.Controller.Async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import haka.ifisol.haka.View.Activities.DeshboardActvty;


/**
 * Created by MEH on 12/27/2016.
 */
public class ProgressBack extends AsyncTask<String,String,String> {
    ProgressDialog PD;
    DeshboardActvty mMenueActivity;
    public ProgressBack(DeshboardActvty mMenueActivity) {
        this.mMenueActivity=mMenueActivity;

    }

    @Override
    protected void onPreExecute() {
        PD= ProgressDialog.show(mMenueActivity,null, "Please Wait ...", true);
        PD.setCancelable(true);
    }

    @Override
    protected String doInBackground(String... arg0) {
        downloadFile("https://www.youtube.com/watch?v=E5ln4uR4TwQ","Video.mp4");
        return null;
    }


    protected void onPostExecute(Boolean result) {
        PD.dismiss();

    }

    private void downloadFile(String fileURL, String fileName) {

        try
        {
            URL url = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Video/");
           /* String PATH = Environment.getExternalStorageDirectory().toString();
            Log.v("LOG_TAG", "PATH: " + PATH);

            File file = new File(PATH);*/
            file.mkdirs();
           // InputStream in = c.getInputStream();
            InputStream is = new BufferedInputStream(url.openStream());
            FileOutputStream f = new FileOutputStream(new File(file, fileName));

            byte[] buff = new byte[5 * 1024];
            int len= -1;
            while ((len = is.read(buff)) != -1)
            {
                f.write(buff, 0, len);
            }
            f.flush();
            f.close();
            is.close();
            PD.dismiss();

/*
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            in.close();*/

        } catch (Exception e) {
            Log.d("Downloader", e.getMessage());
            PD.dismiss();
        }

    }

}

