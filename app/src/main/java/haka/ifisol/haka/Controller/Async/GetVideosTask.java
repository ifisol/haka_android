/*package haka.ifisol.haka.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ifisol.haka.Global.GlobalActivity;
import ifisol.haka.Model.Constants;
import ifisol.haka.Model.Videos;
import ifisol.haka.View.Activities.DeshboardActvty;
import ifisol.haka.View.Fragments.VideoFragment;

*//**
 * Created by MEH on 12/27/2016.
 *//*
public class GetVideosTask extends AsyncTask<String, Void, String> {
    VideoFragment videosMainFragment;
    Context mContext;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    HashMap<String, String> parameters;
    DeshboardActvty Actvty;


    public GetVideosTask(VideoFragment videosMainFragment,DeshboardActvty deshboardActvty, HashMap<String, String> params) {
        this.videosMainFragment=videosMainFragment;
        Actvty=deshboardActvty;
        //mGlobal = (GlobalActivity) Actvty.getApplicationContext();
        jsonArrayParser = new JSONArrayParser();

        Actvty.mViewProgressBar.setVisibility(View.VISIBLE);
        parameters = params;
    }


    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl(parameters, Constants.YOUTUBEBaseURL);

        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        Actvty.mViewProgressBar.setVisibility(View.GONE);
        Videos videos;
        ArrayList<Videos>arrayList=new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);

                JSONArray jsonArray=jsonObject.getJSONArray("items");
                for (int i=0;i<jsonArray.length();i++)
                {
                    videos=new Videos();
                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                    JSONObject jsonObject2=jsonObject1.getJSONObject("snippet");

                    videos.setVideo_title(jsonObject2.getString("title"));
                    JSONObject jsonObject3=jsonObject2.getJSONObject("thumbnails");
                    JSONObject jsonObject4=jsonObject3.getJSONObject("high");
                    videos.setVideo_thumbnail(jsonObject4.getString("url"));
                    JSONObject jsonObject7=jsonObject2.getJSONObject("resourceId");
                    videos.setVideo_id(jsonObject7.getString("videoId"));
                    arrayList.add(videos);

                }

                videosMainFragment.API_RESULT(arrayList);



                } else
                {
                    Toast.makeText(Actvty, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}*/


