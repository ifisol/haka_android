package haka.ifisol.haka.Controller.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Async.JSONArrayParser;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.Model.Youtube;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Fragments.YoutubeVideosFragment;


/**
 * Created by MEH on 1/11/2017.
 */
public class GetYoutubeVideosTask extends AsyncTask<String,Void,String> {
    YoutubeVideosFragment videosFragment;
    private String resp;
    GlobalActivity mGlobal;
    JSONArrayParser jsonArrayParser;
    Context mContext;
    DeshboardActvty actvty;
    HashMap<String, String> parameters;
    ArrayList<Youtube>mArray;
    public GetYoutubeVideosTask(YoutubeVideosFragment frag, DeshboardActvty mMenueActivity, HashMap<String, String> param,String searchthrough) {
        parameters = param;
        videosFragment=frag;
        this.actvty=mMenueActivity;
        mArray=new ArrayList<>();
        jsonArrayParser = new JSONArrayParser();
        if (searchthrough.equalsIgnoreCase("referesh"))
        {
            actvty.mViewProgressBar.setVisibility(View.GONE);
        }
        else
        {
            actvty.mViewProgressBar.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        resp = this.jsonArrayParser.getJSONFromUrl(parameters, Constants.YOUTUBEBaseURL);
        Log.d(" Task Response", resp);
        return resp;
    }

    @Override
    protected void onPostExecute(String result) {
        actvty.mViewProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        try {
            if (!result.isEmpty()||result!=null) {
                jsonObject = new JSONObject(result);

                    JSONArray item=jsonObject.getJSONArray("items");
                    for (int i=0;i<item.length();i++)
                    {
                        JSONObject obj=item.getJSONObject(i);
                        Youtube youtube=new Youtube();
                        JSONObject obj_ID=obj.getJSONObject("id");
                        youtube.setVideo_Yid(obj_ID.getString("videoId"));
                        JSONObject snippet=obj.getJSONObject("snippet");
                        youtube.setVideo_Ytitle(snippet.getString("title"));
                        JSONObject thunbnail=snippet.getJSONObject("thumbnails");
                        JSONObject highSize=thunbnail.getJSONObject("high");
                        youtube.setVideo_Ythunbail(highSize.getString("url"));
                        mArray.add(youtube);
                    }


                    videosFragment.API_RESULT(mArray);

            }
        }
        catch (JSONException e)
        {
            Toast.makeText(actvty, "Internet connection is slow or down.Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
