package haka.ifisol.haka.Controller.Adapters;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.DownloadedVideos;
import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Fragments.DownloadedVideosRecyclerFrag;

/**
 * Created by MEH on 1/30/2017.
 */

public class DownloadsVideosREcyclerAdopter extends RecyclerView.Adapter<DownloadsVideosREcyclerAdopter.MyViewHolder>  {
    private static ClickListener clickListener;
    Context mContext;
    TextView textView;
    TextView tVtime;
    ImageView imageThumbnail;
    ProgressBar progressBar;
    TextView tv_percentag;
    ImageView imgDownload;
    int DonwnLdposition;
    boolean isThreadStarted;
    DownloadedVideosRecyclerFrag donloadfrag;
    GlobalActivity globalActivity;
    ArrayList<DownloadedVideos>videosArray;
    String changItemView;
    View itemView;
    public DownloadsVideosREcyclerAdopter(Context con, ArrayList<DownloadedVideos> fileList, DownloadedVideosRecyclerFrag donloadfragg, String whichfrag)
    {
        videosArray=fileList;
        mContext=con;
        changItemView=whichfrag;
        donloadfrag=donloadfragg;
        globalActivity=(GlobalActivity) mContext.getApplicationContext();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public MyViewHolder(View itemView) {
            super(itemView);
            textView = (TextView)itemView.findViewById(R.id.grid_item_label);
            tVtime = (TextView)itemView.findViewById(R.id.tv_date);
            imageThumbnail = (ImageView)itemView.findViewById(R.id.grid_item_image);
            progressBar=(ProgressBar)itemView.findViewById(R.id.progressbar);
            tv_percentag=(TextView)itemView.findViewById(R.id.tv_percent);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getPosition(),v);
        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (changItemView.equalsIgnoreCase("grid"))
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.downloaedvideos_grid_item, null);
        }
        else if (changItemView.equalsIgnoreCase("linear"))
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.gridlayout_video, null);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder h, int position) {
/*
        if (videosArray.get(position).isDownloading)
        {
            if (videosArray.get(position).getPosition()>=100)
            {
                videosArray.get(position).setDownloading(false);
                videosArray.get(position).setPosition(100);
                progressBar.setVisibility(View.GONE);
                tv_percentag.setVisibility(View.GONE);

            }
            else
            {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(videosArray.get(position).getPosition());
                tv_percentag.setVisibility(View.VISIBLE);
                tv_percentag.setText(videosArray.get(position).getPosition()+"%");
            }

        }
        else
        {
            progressBar.setVisibility(View.GONE);
            tv_percentag.setVisibility(View.GONE);
        }

        if (globalActivity.GetDownloadingVideo()!=null)
        {
            if (!isThreadStarted)
            {
                for (DownloadedVideos obj: videosArray)
                {
                    if (obj.getVideoName().equals(globalActivity.GetDownloadingVideo().getVideoName()))
                        DonwnLdposition = videosArray.indexOf(obj);

                }

                //h.progressBar.setVisibility(View.VISIBLE);

                final DownloadManager manager = (DownloadManager)mContext.getSystemService(Context.DOWNLOAD_SERVICE);
                new Thread(new Runnable() {

                    @Override
                    public void run() {

                        boolean downloading = true;
                        isThreadStarted=true;

                        while (downloading) {

                            DownloadManager.Query q = new DownloadManager.Query();
                            q.setFilterById(globalActivity.GetDownloadingVideo().getVideoID());
                            Cursor cursor = manager.query(q);
                            cursor.moveToFirst();
                            final int bytes_downloaded = cursor.getInt(cursor
                                    .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                            int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                            if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                                downloading = false;
                            }

                            final int dl_progress = (int) ((double)bytes_downloaded / (double)bytes_total * 100f);

                            videosArray.get(DonwnLdposition).setDownloading(true);
                            videosArray.get(DonwnLdposition).setPosition(dl_progress);

                            try {
                                Thread.sleep(5000);
                                donloadfrag.getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        notifyDataSetChanged();
                                        if (dl_progress>=100)
                                        {
                                            Thread.currentThread().interrupt();
                                            globalActivity.SaveDownloadingVideo(null);
                                            isThreadStarted=false;
                                        }


                                    }

                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            cursor.close();
                        }

                    }
                }).start();

            }
        }*/


        textView.setText(videosArray.get(position).getVideoName());
        imageThumbnail.setImageBitmap(videosArray.get(position).getVideoThunmbNail());
        //tVtime.setText(videosArray.get(position).getVideoDate());

    }

    @Override
    public int getItemCount() {
        return videosArray.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public interface ClickListener {
        void onItemClick(int position, View v);

    }
    public void setOnItemClickListener(ClickListener clickListener) {
        DownloadsVideosREcyclerAdopter.clickListener = clickListener;
    }
}
