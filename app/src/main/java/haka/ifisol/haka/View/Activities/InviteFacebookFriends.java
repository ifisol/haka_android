package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;

import haka.ifisol.haka.R;

/**
 * Created by MEH on 1/18/2017.
 */

public class InviteFacebookFriends extends Activity {
    private CallbackManager sCallbackManager;
    String AppURl = "https://fb.me/1838183356470843";

    String previewImageUrl = "https://www.google.com.pk/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwi2qJ-66pvRAhWCxxQKHcE4DR0QjRwIBw&url=https%3A%2F%2Fwww.youtube.com%2Fuser%2FGoogle&psig=AFQjCNGN_vz72oFKnmv6QFOIHEwb3NkFzQ&ust=1483184355216687";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_fb_friends);
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        sCallbackManager = CallbackManager.Factory.create();
        AppInviteDialog appInviteDialog = new AppInviteDialog(this);
        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(AppURl)/*.setPreviewImageUrl(previewImageUrl)*/
                    .build();
            appInviteDialog.registerCallback(sCallbackManager,
                    new FacebookCallback<AppInviteDialog.Result>() {
                        @Override
                        public void onSuccess(AppInviteDialog.Result result) {
                            Toast.makeText(InviteFacebookFriends.this, "Successfully sent", Toast.LENGTH_SHORT).show();

                            Log.d("Invitation", "Invitation Sent Successfully");
                            finish();
                        }

                        @Override
                        public void onCancel() {
                            Toast.makeText(InviteFacebookFriends.this, "Invitation Cancel", Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onError(FacebookException e) {
                            Toast.makeText(InviteFacebookFriends.this, "Invitation not sent", Toast.LENGTH_SHORT).show();

                            Log.d("Invitation", "Error Occured");
                        }
                    });

            appInviteDialog.show(content);

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sCallbackManager.onActivityResult(requestCode, resultCode, data);
        finish();

    }
}
