package haka.ifisol.haka.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gun0912.tedpicker.ImagePickerActivity;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Adapters.SettingsAdopter;
import haka.ifisol.haka.Controller.Async.UpDateProfileTask;
import haka.ifisol.haka.Controller.Async.UpdatePrifileImag;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Activities.DeshboardActvty;

/**
 * Created by MEH on 1/30/2017.
 */

public class Fragment_new_setting extends Fragment implements View.OnClickListener {
    TabLayout tabLayoutsetting;
    ViewPager vPagersetting;
    SettingsAdopter settingsAdopter;
    Context context;
    ImageView img_profil;
    public static TextView tvName;
    GlobalActivity mGlobal;
    boolean isClicked = true;
    RelativeLayout viewChangProf;
    Bitmap UserImage = null;
    DeshboardActvty mMenueActivity;
    boolean isNeverAskAgainGallery = false;
    boolean isNeverAskAgainCamera = false;
    int PHOTO_PICKER_ID = 10;
    public int REQUEST_CODE_PICKER = 10;
    Fragment_new_setting frag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        return Init(view);

    }

    private View Init(View view) {
        vPagersetting = (ViewPager) view.findViewById(R.id.viewpager_setting);
        tabLayoutsetting = (TabLayout) view.findViewById(R.id.tabs_setting);
        img_profil = (ImageView) view.findViewById(R.id.img_prf);
        viewChangProf = (RelativeLayout) view.findViewById(R.id.view_chng_prof);
        context = getActivity();
        mMenueActivity=(DeshboardActvty)getActivity();
        mGlobal = (GlobalActivity) context.getApplicationContext();
        DeshboardActvty.tvHead.setText("Settings");
        SetData();
        frag=this;
        img_profil.setOnClickListener(this);
        viewChangProf.setOnClickListener(this);
        return view;

    }

    public void SetData() {
        // tvName.setText(mGlobal.GetActiveUser().getUser_name());
        String img = mGlobal.GetActiveUser().getPicture();
        if (mGlobal.GetActiveUser().getFacebookUser().equals("no")) {
            if (mGlobal.GetActiveUser().getPicture() != null || mGlobal.GetActiveUser().getPicture().length() > 0) {
                Picasso.with(context)
                        .load(Constants.Img_URL + img)
                        .error(R.drawable.dummy_profile_pic)
                        .into(img_profil);

            }

        }
        if (mGlobal.GetActiveUser().getFacebookUser().equals("yes")) {
            String imgg = mGlobal.GetActiveUser().getPicture();
            if (mGlobal.GetActiveUser().getPicture() != null && mGlobal.GetActiveUser().getPicture().length() > 0) {
                Picasso.with(context)
                        .load(mGlobal.GetActiveUser().getPicture())
                        .error(R.drawable.dummy_profile_pic)
                        .into(img_profil);
            }

        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        settingsAdopter = new SettingsAdopter(getChildFragmentManager(), context);
        vPagersetting.setAdapter(settingsAdopter);
        tabLayoutsetting.setupWithViewPager(vPagersetting);
        for (int i = 0; i < tabLayoutsetting.getTabCount(); i++) {
            tabLayoutsetting.getTabAt(i).setCustomView(settingsAdopter.getTabView(i));
        }

        TabLayout.Tab tab = tabLayoutsetting.getTabAt(0);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setColorFilter(getActivity().getResources().getColor(R.color.haka_them_color));


        tabLayoutsetting.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getActivity().getResources().getColor(R.color.haka_them_color));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getActivity().getResources().getColor(R.color.grey));

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_prf:
                if (isClicked) {
                    viewChangProf.setVisibility(View.VISIBLE);
                    isClicked = false;
                } else {
                    viewChangProf.setVisibility(View.GONE);
                    isClicked = true;
                }
                break;
            case R.id.view_chng_prof:
                if (mGlobal.GetActiveUser().getFacebookUser().equals("no"))
                {
                        imgPickerDialog();

                }
                else {
                    Utility.ShowAlertDialog(getActivity(), "Alert", "You can not change your profile");

                }

                break;
        }
    }

    private void imgPickerDialog() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (isNeverAskAgainGallery) {
                ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Storage permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions", "Storage Permission Required");
            } else
                frag.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
        } else {
            if (ContextCompat.checkSelfPermission(mMenueActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            {
                if (isNeverAskAgainCamera) {
                    ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Camera permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions", "Camera Permission Required");
                } else
                {
                    frag.requestPermissions(new String[]{Manifest.permission.CAMERA},4);
            }

        }
            else {
                Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
                startActivityForResult(intent, PHOTO_PICKER_ID);
            }
        }
    }

    private void ShowPermissionWarning(final String s, String s1, String s2) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog.findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView) dialog.findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView) dialog.findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        tvHeading.setText(s2);
        tvDesc.setText(s1);
        tvRetry.setText(s);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (s.equalsIgnoreCase("retry")) {
                    CheckPermission();
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void CheckPermission() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 5:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /*Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("image*//*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 1);
                    dialog.dismiss();*/
                    if (ContextCompat.checkSelfPermission(mMenueActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (isNeverAskAgainCamera) {
                            ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Camera permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions", "Camera Permission Required");
                        } else
                            frag.requestPermissions(new String[]{Manifest.permission.CAMERA}, 4);
                    } else {
                        Intent intent = new Intent(mMenueActivity, ImagePickerActivity.class);
                        startActivityForResult(intent, PHOTO_PICKER_ID);
                    }

                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mMenueActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mMenueActivity, permissions[0])) {
                        isNeverAskAgainGallery = true;
                        // ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    } else {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }

                break;
            case 4:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(mMenueActivity, ImagePickerActivity.class);
                    startActivityForResult(intent, PHOTO_PICKER_ID);
                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 0);*/
                    //dialog.dismiss();
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mMenueActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mMenueActivity, permissions[0])) {
                        isNeverAskAgainCamera = true;
                        // ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    } else {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (requestCode == PHOTO_PICKER_ID && resultCode == Activity.RESULT_OK) {
            ArrayList<Uri> image_uris = imageReturnedIntent.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
            try {
                String uri = image_uris.get(0).toString();
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    UserImage = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    img_profil.setBackgroundResource(0);
                    img_profil.setImageBitmap(UserImage);
                    String img="";
                    if (UserImage!=null)
                    {
                        HashMap<String, String> param = new HashMap<>();
                        img= Utility.EncodeTobase64(UserImage);
                        img = Utility.EncodeTobase64(UserImage);
                        param.put("photo", img);
                        param.put("user_id", mGlobal.GetActiveUser().getUser_id());
                        param.put("user_name",mGlobal.GetActiveUser().getUser_name());
                        param.put("email",mGlobal.GetActiveUser().getUser_email());
                        new UpdatePrifileImag(param, mMenueActivity, frag).execute();

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void API_RESULT() {
        SetData();
        viewChangProf.setVisibility(View.GONE);
        isClicked = true;
    }
}
