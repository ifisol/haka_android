package haka.ifisol.haka.View.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.esafirm.imagepicker.model.Image;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Async.SignupTask;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;


public class SignUpActivity extends Activity implements View.OnClickListener {
    public static View mViewProgressBar;
    Context mContext;
    EditText eUserNam;
    EditText eUserMail;
    EditText eUserPass;
    EditText edtConfirmPass;
    Button mbtRegister;
    SignUpActivity mActivity;
    View vReisterWithFb;
    ImageView imgprfl;
    ImageView addProfil;
    /*CircularImageView imgprfl;*/
    Dialog dialog;
    View viewBack;
    //TextView back;
    boolean isNeverAskAgainGallery=false;
    boolean isNeverAskAgainCamera=false;
    Bitmap UserImage=null;
    String img="";
    int PHOTO_PICKER_ID=10;
    public int REQUEST_CODE_PICKER=10;
    ImageView imgBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_redesign);
        mActivity = this;
        Init();
    }

    private void Init() {
        mContext = this;
        edtConfirmPass=(EditText) findViewById(R.id.edit_con_pass);
        viewBack=(View) findViewById(R.id.view_login_back);
        addProfil=(ImageView) findViewById(R.id.add_profil);
        //imgBack=(ImageView) findViewById(R.id.img_bak);
        //back=(TextView)findViewById(R.id.tv_backlogin);
        /*imgprfl=(CircularImageView)findViewById(R.id.img_profilee);*/
        imgprfl=(ImageView) findViewById(R.id.img_profilee);
        mViewProgressBar = (View) findViewById(R.id.view_progrss);
        eUserNam = (EditText) findViewById(R.id.edt_usernam);
        eUserMail = (EditText) findViewById(R.id.edt_useremail);
        eUserPass = (EditText) findViewById(R.id.edt_userpassword);
        mbtRegister = (Button) findViewById(R.id.bt_register);
        vReisterWithFb=(View)findViewById(R.id.view_facbook);
        SetOnClickListener();
    }

    private void SetOnClickListener() {
        mbtRegister.setOnClickListener(this);
        vReisterWithFb.setOnClickListener(this);
        addProfil.setOnClickListener(this);
       /// imgBack.setOnClickListener(this);
        viewBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bt_register:
                RegisterUser();
                break;
            case R.id.view_facbook:
                Intent intent = new Intent(this,RegisterWithFacebookActivity.class);
                intent.putExtra("value", "register");
                Utility.OpenActivityNextByData(intent,mActivity);
                break;
            case R.id.add_profil:
                imgPickerDialog();
                //selectDialog();
                break;
            case R.id.tv_backlogin:
                Utility.OpenActivityPrevious(mContext, SignUpActivity.this, LoginActivity.class);
                break;
            case R.id.view_login_back:
                Intent i = new Intent(SignUpActivity.this,  LoginActivity.class);
                startActivity(i);
                break;
        }
    }

    private void imgPickerDialog() {
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (isNeverAskAgainGallery) {
                ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Storage permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions","Storage Permission Required");
            } else
                ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
        } else {
            if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (isNeverAskAgainCamera) {
                    ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Camera permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions","Camera Permission Required");
                } else
                    ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, 4);
            }
            else {
                Intent intent = new Intent(mActivity, ImagePickerActivity.class);
                startActivityForResult(intent, PHOTO_PICKER_ID);
            }

        }
    }

   /* private void selectDialog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.custom_profile_image_picker_dialog);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        Button mbtGallery = (Button) dialog.findViewById(R.id.bt_gallery);
        Button mbtCamera = (Button) dialog.findViewById(R.id.bt_camera);
        mbtCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (isNeverAskAgainCamera) {
                        ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Camera permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    } else
                        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, 4);
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 0);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
        mbtGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED )
                {
                    if (isNeverAskAgainGallery)
                    {
                        ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Storage permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    }
                    else
                        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
                }
                else
                {
                   *//* Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("image*//**//*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 0);
                    dialog.dismiss();*//*
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, REQUEST_CODE_PICKER);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }*/

    private void ShowPermissionWarning(final String s, String s1, String s2) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog. findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView)    dialog. findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView)   dialog. findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView)  dialog. findViewById(R.id.tv_cancel);

        tvHeading.setText(s2);
        tvDesc.setText(s1);
        tvRetry.setText(s);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(s.equalsIgnoreCase("retry"))
                {
                    CheckPermission();
                }
                else
                {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        }); dialog.show();
    }

    private void CheckPermission() {

    }

    private void RegisterUser() {
        boolean isValid = true;
        if (eUserNam.getText().toString().isEmpty()) {
            if (isValid) {
                Utility.ShowAlertDialog(mActivity, "User Name", "Please enter user name");
                isValid = false;
            }
        }
        if (eUserMail.getText().toString().isEmpty()) {
            if (isValid) {
                Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
                isValid = false;
            }
        }
        if (!Utility.EmailValidator(eUserMail.getText().toString())) {
            if (isValid) {
                Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
                isValid = false;
            }
        }
        if (eUserPass.getText().toString().isEmpty()) {
            if (isValid)
                Utility.ShowAlertDialog(mActivity, "Password", "Please enter password");
            isValid = false;
        }

        if (eUserPass.getText().toString().length()<6){
            if (isValid) {
                Utility.ShowAlertDialog(mActivity, "Password", "Password should contain at least 6 character");
                isValid = false;
            }
        }
        if (edtConfirmPass.getText().toString().isEmpty())
        {
            if (isValid)
                Utility.ShowAlertDialog(mActivity, "Password", "Please enter Confirm password");
            isValid = false;
        }
        if (!eUserPass.getText().toString().equals(edtConfirmPass.getText().toString()))
        {
            if (isValid)
                Utility.ShowAlertDialog(mActivity, "Password", "Password doesn't match");
            isValid = false;
        }
        if (isValid) {

            String img="";
            if (UserImage!=null)
            {
                img=Utility.EncodeTobase64(UserImage);
            }


            HashMap<String, String> param = new HashMap<>();
            param.put("photo", img);
            param.put("user_name", eUserNam.getText().toString());
            param.put("email", eUserMail.getText().toString());
            param.put("password", eUserPass.getText().toString());
            param.put("facebook", "no");
            param.put("device_type", "Android");
            param.put("device_token", "");

            new SignupTask(SignUpActivity.this, param).execute();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(SignUpActivity.this,  LoginActivity.class);
        startActivity(i);
    }

    public void API_RESULT() {
        Utility.OpenActivityPrevious(mContext, SignUpActivity.this, DeshboardActvty.class);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 5:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /*Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("image*//*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 1);
                    dialog.dismiss();*/
                    if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (isNeverAskAgainCamera) {
                            ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Camera permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions","Camera Permission Required");
                        } else
                            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, 4);
                    }
                    else {
                        Intent intent = new Intent(mActivity, ImagePickerActivity.class);
                        startActivityForResult(intent, PHOTO_PICKER_ID);
                    }

                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0])) {
                        isNeverAskAgainGallery = true;
                        // ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    } else {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }

                break;
            case 4:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(mActivity, ImagePickerActivity.class);
                    startActivityForResult(intent, PHOTO_PICKER_ID);
                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 0);*/
                    //dialog.dismiss();
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0])) {
                        isNeverAskAgainCamera = true;
                        // ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Location permission is compulsory for Fit Exchange. You can still allow permission from settings. Go to settings and select permissions");
                    } else {
                        //ShowPermissionWarning("Retry","Please note that Fit Exchange works on the basis of location services like displaying posts and creating new posts against a specific location.Fit Exchange wont be able to work without location permission. ");
                    }
                }
                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (requestCode == PHOTO_PICKER_ID && resultCode == Activity.RESULT_OK) {
            ArrayList<Uri> image_uris = imageReturnedIntent.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
            try {
                String uri = image_uris.get(0).toString();
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    UserImage = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imgprfl.setBackgroundResource(0);
                    imgprfl.setImageBitmap(UserImage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       /* if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {

            Uri selectedImage = data.getData();
            try {
                UserImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                int dimension = getSquareCropDimensionForBitmap(UserImage);
                UserImage = ThumbnailUtils.extractThumbnail(UserImage, dimension, dimension);
                *//*if (UserImage != null) {
                    imgprfl.setImageResource(0);
                    imgprfl.setImageBitmap(UserImage);
                }*//*
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Uri imageUri = null;
            try {
                imageUri = data.getData();
            } catch (NullPointerException e) {
            }
            super.onActivityResult(requestCode, resultCode, data);
            Intent intent;
            switch (requestCode) {
                case 1:
                    if (resultCode == Activity.RESULT_OK) {
                        try {
                            Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
                            Crop.of(imageUri, destination).asSquare().start(mActivity);
                            //          CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).start(this);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    break;
                case 0:

                    if (resultCode == Activity.RESULT_OK) {
                        try {
                            UserImage = (Bitmap) data.getExtras().get("data");
                            int dimension = getSquareCropDimensionForBitmap(UserImage);
                            UserImage = ThumbnailUtils.extractThumbnail(UserImage, dimension, dimension);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    break;
                case Crop.REQUEST_CROP:

                    if (resultCode == Activity.RESULT_OK) {
                        Uri resultUri = Crop.getOutput(data);

                        try {
                            UserImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    break;

                case Crop.RESULT_ERROR:
                    Toast.makeText(this, Crop.getError(data).getMessage(), Toast.LENGTH_SHORT).show();
                    break;

            }

            if (UserImage != null) {

                imgprfl.setImageResource(0);
                imgprfl.setImageBitmap(UserImage);
            }

        }*/
    }

}


