/*package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import haka.ifisol.haka.R;
import ifisol.haka.Controller.Async.LoginTask;
import ifisol.haka.Controller.Async.LoginWithFacebookTask;
import ifisol.haka.Utilities.Utility;

*//**
 * Created by MEH on 1/9/2017.
 *//*
public class LoginWithFacebookActivity extends Activity {
    Context context;
    LoginManager loginManager;
    private CallbackManager callbackManager;

    String profileimgURL;
    String email;
    String name;
    Button bttRegister;
    public static View mViewProgressBar;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_login);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        init();
        loginManager = LoginManager.getInstance();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        progress=new ProgressDialog(LoginWithFacebookActivity.this);
        progress.setMessage("please wait");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        loginManager.logInWithReadPermissions(LoginWithFacebookActivity.this, Arrays.asList("public_profile,user_friends,email"));


        callbackManager = CallbackManager.Factory.create();
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                 Toast.makeText(LoginWithFacebookActivity.this, "success", Toast.LENGTH_SHORT).show();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                setProfileToView(object);
                                Log.v("Main", response.toString());
                                if(loginResult.getAccessToken() != null){
                                    LoginManager.getInstance().logOut();
                                }
                                Toast.makeText(LoginWithFacebookActivity.this, "complet", Toast.LENGTH_SHORT).show();

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,picture.width(150).height(150)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Utility.OpenActivityPrevious(context, LoginWithFacebookActivity.this, LoginActivity.class);
                Toast.makeText(LoginWithFacebookActivity.this, "Registeration cancel", Toast.LENGTH_SHORT).show();
                progress.dismiss();
            }

            @Override
            public void onError(FacebookException error) {
                Utility.OpenActivityPrevious(context, LoginWithFacebookActivity.this, LoginActivity.class);
                Toast.makeText(LoginWithFacebookActivity.this, "There is an error please try again later", Toast.LENGTH_SHORT).show();
                progress.dismiss();
            }
        });

    }

    private void init() {
        mViewProgressBar = (View) findViewById(R.id.view_progrss_fblogin);
        context=this;
    }

    private void setProfileToView(JSONObject object) {

        if (!(object ==null)) {
            try {
                email=object.getString("email");
                name=object.getString("name");
                profileimgURL = object.getJSONObject("picture").getJSONObject("data").getString("url");

                HashMap<String, String> param = new HashMap<>();
                param.put("email", email.toString());
                param.put("facebook", "yes");
                param.put("device_type", "Android");
                param.put("device_token", "");

                new LoginWithFacebookTask(LoginWithFacebookActivity.this, param).execute();

                loginManager.logOut();

            } catch (JSONException e) {
                Utility.OpenActivityPrevious(context, LoginWithFacebookActivity.this, LoginActivity.class);
                Toast.makeText(LoginWithFacebookActivity.this, "There is an error please try again later", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

        }
        else
        {
            Toast.makeText(LoginWithFacebookActivity.this, "There is an error please try again later", Toast.LENGTH_SHORT).show();
            Utility.OpenActivityPrevious(context, LoginWithFacebookActivity.this, LoginActivity.class);

        }
    }

    public void API_RESULT() {
        Utility.OpenActivityNext(context, LoginWithFacebookActivity.this,DeshboardActvty.class);
    }
}*/
