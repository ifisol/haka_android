package haka.ifisol.haka.View.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpicker.ImagePickerActivity;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Async.ChangePasswordTask;
import haka.ifisol.haka.Controller.Async.UpDateProfileTask;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Constants;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Activities.InviteFacebookFriends;
import haka.ifisol.haka.View.Activities.LoginActivity;
import haka.ifisol.haka.View.Activities.ShareLinkActivity;

import static android.app.Activity.RESULT_OK;

/**
 * Created by MEH on 12/22/2016.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {

    Context mContext;
    ImageView addProfile;
    EditText edtUserName;
    TextView edtUserEmail;
    TextView tvChangPassword;
    Button btedtProfil;
    Button btUpdatProfil;
    GlobalActivity mGlobal;
    ImageView imgProf;
    EditText edConfirmPass;
    String newPass;
    TextView rvLogout;
    Bitmap UserImage=null;
    SettingsFragment fragmenttt;
    DeshboardActvty mMenueActivity;
    boolean isNeverAskAgainGallery=false;
    boolean isNeverAskAgainCamera=false;
    View viewShareFacebook;
    int PHOTO_PICKER_ID=10;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment_redesign, container, false);
        fragmenttt=this;
        return Init(view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private View Init(View view) {
        DeshboardActvty.imgBack.setVisibility(View.GONE);
        mMenueActivity=(DeshboardActvty)getActivity();
        mContext=getActivity();
        DeshboardActvty.tvHead.setText("PROFILE");
        viewShareFacebook=(View) view.findViewById(R.id.view_facbook);
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
        imgProf=(ImageView)view.findViewById(R.id.img_profileee);
        addProfile=(ImageView)view.findViewById(R.id.add_profil);
        DeshboardActvty.imgFilter.setVisibility(View.GONE);
        edtUserName=(EditText)view.findViewById(R.id.edt_user_name);
        edtUserEmail=(TextView) view.findViewById(R.id.edt_user_email);
        tvChangPassword =(TextView)view.findViewById(R.id.tv_change_pass);
        btedtProfil=(Button)view.findViewById(R.id.bt_edt_profile);
        btUpdatProfil=(Button)view.findViewById(R.id.bt_update_profile);
        rvLogout=(TextView)view.findViewById(R.id.tv_logout);
        setData();


        SetOnClickListener();
        DisablProfilFields();
        return view;
    }

    private void setData() {

        edtUserName.setText(mGlobal.GetActiveUser().getUser_name());
        edtUserEmail.setText(mGlobal.GetActiveUser().getUser_email());
        String img = mGlobal.GetActiveUser().getPicture();
        String s=mGlobal.GetActiveUser().getFacebookUser();
        if (mGlobal.GetActiveUser().getFacebookUser().equals("no")) {
            if (mGlobal.GetActiveUser().getPicture() != null || mGlobal.GetActiveUser().getPicture().length() > 0) {
                Picasso.with(mContext)
                        .load(Constants.Img_URL + img)
                        //.error(R.drawable.capture)
                        .into(imgProf);

            }

        }
        if (mGlobal.GetActiveUser().getFacebookUser().equals("yes")) {
           // String imgg = mGlobal.GetActiveUser().getPicture();
            if (GlobalActivity.GetFacebookUserImagURL() != null || mGlobal.GetActiveUser().getPicture().length() > 0) {
                Picasso.with(mContext)
                        .load(GlobalActivity.GetFacebookUserImagURL())
                        .into(imgProf);
            }


        }
    }


    private void SetOnClickListener() {
        btedtProfil.setOnClickListener(this);
        tvChangPassword.setOnClickListener(this);
        rvLogout.setOnClickListener(this);
        btUpdatProfil.setOnClickListener(this);
        addProfile.setOnClickListener(this);
        viewShareFacebook.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.bt_edt_profile:
                if (mGlobal.GetActiveUser().getFacebookUser().equals("no"))
                {
                    EnablProfilFields();

                }
                else {
                    Utility.ShowAlertDialog(getActivity(), "Alert", "You can not change your profile");

                }
                break;
            case R.id.bt_update_profile:
                upDateProfile();
                break;
            case R.id.tv_change_pass:
                changPassDialog();
                break;
            case R.id.tv_logout:
                mGlobal.SaveActiveUser(null);
                Utility.OpenActivityPrevious(mContext, getActivity(), LoginActivity.class);
                break;
            case R.id.add_profil:
                SelectImageDialid();
                break;
            case R.id.view_facbook:
                Utility.OpenActivityNext(mContext,getActivity(),ShareLinkActivity.class);
                break;

        }
    }

    private void SelectImageDialid() {
        if (ContextCompat.checkSelfPermission(mMenueActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (isNeverAskAgainGallery) {
                ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Storage permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions","Storage Permission Required");
            } else
                ActivityCompat.requestPermissions(mMenueActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
        } else {
            if (ContextCompat.checkSelfPermission(mMenueActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (isNeverAskAgainCamera) {
                    ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Camera permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions","Camera Permission Required");
                } else
                    ActivityCompat.requestPermissions(mMenueActivity, new String[]{Manifest.permission.CAMERA}, 4);
            }
            else {
                Intent intent = new Intent(mMenueActivity, ImagePickerActivity.class);
                startActivityForResult(intent, PHOTO_PICKER_ID);
            }

        }
    }

    private void ShowPermissionWarning(final String s, String s1, String s2) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog. findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView)    dialog. findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView)   dialog. findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView)  dialog. findViewById(R.id.tv_cancel);

        tvHeading.setText(s2);
        tvDesc.setText(s1);
        tvRetry.setText(s);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(s.equalsIgnoreCase("retry"))
                {
                    CheckPermission();
                }
                else
                {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        }); dialog.show();
    }

    private void CheckPermission() {

    }

    private void changPassDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.change_password_dialog);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final EditText edCurrentPass = (EditText) dialog.findViewById(R.id.edt_current_password);
        final EditText edNewPass = (EditText) dialog.findViewById(R.id.edt_new_password);
        edConfirmPass = (EditText) dialog.findViewById(R.id.edt_confirm_password);
        Button mBChagPass = (Button) dialog.findViewById(R.id.bt_chang_pass);
        Button mBCancel = (Button) dialog.findViewById(R.id.bt_camcel_pass);
        mBChagPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isValid = true;
                if (edCurrentPass.getText().toString().isEmpty()) {
                    Utility.ShowAlertDialog(getActivity(), "Password", "Please enter current password");
                    isValid = false;
                }
                if (edNewPass.getText().toString().isEmpty()) {
                    if (isValid) {
                        Utility.ShowAlertDialog(getActivity(), "Password", "Please enter new password");
                        isValid = false;
                    }
                }
                if (edNewPass.getText().toString().length() < 6) {
                    if (isValid) {
                        Utility.ShowAlertDialog(getActivity(), "Password", "Password should contain at least 6 character");
                        isValid = false;
                    }
                }
                if (edConfirmPass.getText().toString().isEmpty()) {
                    if (isValid) {
                        Utility.ShowAlertDialog(getActivity(), "Password", "Please enter confirm password");
                        isValid = false;
                    }
                }
                if (!edNewPass.getText().toString().equals(edConfirmPass.getText().toString())) {
                    if (isValid) {
                        Utility.ShowAlertDialog(getActivity(), "Password", "Password doesn?t match");
                        isValid = false;
                    }
                }
                if (isValid) {
                    //APP CALLING
                    String oldPass = edCurrentPass.getText().toString();
                    newPass = edConfirmPass.getText().toString();
                    String User_id = mGlobal.GetActiveUser().getUser_id();
                    HashMap<String, String> param = new HashMap<>();
                    param.put("user_id", User_id);
                    param.put("password", oldPass);
                    param.put("new-password", newPass);
                    new ChangePasswordTask(mMenueActivity, param).execute();
                    dialog.dismiss();
                }

            }
               });
             mBCancel.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

            dialog.dismiss();
        }
    });
    dialog.show();

    }


    private void upDateProfile() {
        boolean isValid=true;
        if (edtUserName.getText().toString().isEmpty()) {
            if (isValid) {
                Utility.ShowAlertDialog(getActivity(), "User Name", "Please enter user name");
                isValid = false;
            }
        }
        if (isValid)
        {
            HashMap<String, String> param = new HashMap<>();
                String img = "";
                if (UserImage != null) {
                    img = Utility.EncodeTobase64(UserImage);
                }
                param.put("photo", img);
                param.put("user_id", mGlobal.GetActiveUser().getUser_id());
                param.put("user_name", edtUserName.getText().toString());
                param.put("email",edtUserEmail.getText().toString());
                new UpDateProfileTask(param, mMenueActivity, fragmenttt).execute();

            }
        
    }
    private void EnablProfilFields() {
        addProfile.setEnabled(true);
        edtUserName.setEnabled(true);
        edtUserEmail.setEnabled(false);
        tvChangPassword.setEnabled(true);
        btedtProfil.setVisibility(View.GONE);
        btUpdatProfil.setVisibility(View.VISIBLE);
    }
    private void DisablProfilFields() {
        addProfile.setEnabled(false);
        edtUserName.setEnabled(false);
        edtUserEmail.setEnabled(false);
        tvChangPassword.setEnabled(false);
        btedtProfil.setVisibility(View.VISIBLE);
        btUpdatProfil.setVisibility(View.GONE);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 5:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(mMenueActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (isNeverAskAgainCamera) {
                            ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Camera permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions","Camera Permission Required");
                        } else
                            ActivityCompat.requestPermissions(mMenueActivity, new String[]{Manifest.permission.CAMERA}, 4);
                    }
                    else {
                        Intent intent = new Intent(mMenueActivity, ImagePickerActivity.class);
                        startActivityForResult(intent, PHOTO_PICKER_ID);
                    }

                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mMenueActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mMenueActivity, permissions[0])) {
                        isNeverAskAgainGallery = true;
                    } else {
                    }
                }

                break;
            case 4:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(mMenueActivity, ImagePickerActivity.class);
                    startActivityForResult(intent, PHOTO_PICKER_ID);
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mMenueActivity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(mMenueActivity, permissions[0])) {
                        isNeverAskAgainCamera = true;
                    } else {
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (requestCode == PHOTO_PICKER_ID && resultCode == Activity.RESULT_OK) {
            ArrayList<Uri> image_uris = imageReturnedIntent.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
            try {
                String uri = image_uris.get(0).toString();
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    UserImage = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    if (UserImage != null) {

                        imgProf.setImageResource(0);
                        imgProf.setImageBitmap(UserImage);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void API_RESULT() {
        setData();
        DisablProfilFields();
    }
}
