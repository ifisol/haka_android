/*
package haka.ifisol.haka.View.Fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Activities.DeshboardActvty;


*/
/**
 * Created by MEH on 12/20/2016.
 *//*

public class NewsFragment extends Fragment {
    GlobalActivity mGlobal;
    Context mContext;
    public WebView webView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        return Init(view);
    }

    private View Init(View view) {
        mContext = getActivity();
        mGlobal = (GlobalActivity) mContext.getApplicationContext();
        webView = (WebView) view.findViewById(R.id.webview_news);
        //DeshboardActvty.imGFilter.setVisibility(View.VISIBLE);
        DeshboardActvty.tvHead.setText("News");
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://www.nzherald.co.nz/");
        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    WebView webView = (WebView) v;

                    switch(keyCode)
                    {
                        case KeyEvent.KEYCODE_BACK:
                            if(webView.canGoBack())
                            {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }

                return false;
            }
        });
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            DeshboardActvty.mViewProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            DeshboardActvty.mViewProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }
    }

}
*/
