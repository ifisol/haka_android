package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;

/**
 * Created by MEH on 3/27/2017.
 */
public class ShareLinkActivity extends Activity implements View.OnClickListener {
    ImageView imgTwitter;
    ImageView imgFacebook;
    EditText edt_referal_code;
    TextView tv_CopyCode;
    ImageView img_Mssag;
    ImageView img_Back;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_link);
        init();
    }

    private void init() {
        mContext=this;
        imgTwitter=(ImageView)findViewById(R.id.img_twitter);
        imgFacebook=(ImageView)findViewById(R.id.img_facebook);
        edt_referal_code=(EditText) findViewById(R.id.edt_referal_cod);
        tv_CopyCode=(TextView) findViewById(R.id.tv_copy_code);
        img_Mssag=(ImageView) findViewById(R.id.img_messag);
        img_Back=(ImageView) findViewById(R.id.img_back);

        edt_referal_code.setText("https://play.google.com/store/apps/details?id=infinitevisio.attendahl=enreferrer=007");
        imgFacebook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        tv_CopyCode.setOnClickListener(this);
        img_Mssag.setOnClickListener(this);
        img_Back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_facebook:

                Utility.OpenActivityNext(mContext,ShareLinkActivity.this,ShareReferalCodeFacebookActivity.class);
                break;
            case R.id.img_twitter:

                Utility.OpenActivityNext(mContext,ShareLinkActivity.this,ShareReferalCodeTwitterActvitiy.class);
                break;
            case R.id.tv_copy_code:

                int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    if (!edt_referal_code.getText().toString().equals(""))
                    {
                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboard.setText(edt_referal_code.getText().toString());
                        Toast.makeText(mContext,"Coppied",Toast.LENGTH_LONG).show();
                    }

                } else {
                    if (!edt_referal_code.getText().toString().equals(""))
                    {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        android.content.ClipData clip = android.content.ClipData.newPlainText("text label",edt_referal_code.getText().toString());
                        clipboard.setPrimaryClip(clip);
                        Toast.makeText(mContext,"Coppied",Toast.LENGTH_LONG).show();
                    }

                }                break;
            case R.id.img_back:
                finish();
                break;
            case R.id.img_messag:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
                {
                    String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); // Need to change the build to API 19

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=infinitevisio.attendahl=enreferrer=007");

                    if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
                    // any app that support this intent.
                    {
                        sendIntent.setPackage(defaultSmsPackageName);
                    }
                    startActivity(sendIntent);

                }
                else {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.setType("vnd.android-dir/mms-sms");
                    intent.putExtra("address","");
                    intent.putExtra("jhjh", "https://play.google.com/store/apps/details?id=infinitevisio.attendahl=enreferrer=007");
                    startActivity(intent);
                }

                break;
        }
    }
}
