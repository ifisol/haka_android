/*
package haka.ifisol.haka.View.Fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.DailyMotion;
import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Activities.DMWebVideoView;
import haka.ifisol.haka.View.Activities.DeshboardActvty;

*/
/**
 * Created by MEH on 1/11/2017.
 *//*

public class DailyMotionPlayerFragment extends Fragment {
    GlobalActivity mGlobal;
    Context mContext;
    public WebView webView;
    String VIDEO_ID;
    private DMWebVideoView mVideoView;
   // String BaseURL="http://www.dailymotion.com/video/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dailymotionplyer, container, false);
        return Init(view);

    }

    private View Init(View view) {
        Bundle bundl = getArguments();
        DailyMotion videos= (DailyMotion) bundl.getSerializable("video_id");
        VIDEO_ID =videos.getVideoID();
        mContext = getActivity();
        mGlobal = (GlobalActivity) mContext.getApplicationContext();
        DeshboardActvty.tvHead.setText("DailyMotion");
        mVideoView = ((DMWebVideoView)view.findViewById(R.id.dmWebVideoView));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String baseURL="http://www.dailymotion.com/embed/video/";
        String fullscreenView="?api=location&fullscreen-action=trigger_event&autoplay=1";
        String s=baseURL+""+VIDEO_ID+""+fullscreenView;
        mVideoView.loadUrl(baseURL+""+VIDEO_ID+""+fullscreenView);

    }

    @Override
    public void onPause() {
        super.onPause();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            mVideoView.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            mVideoView.onResume();
        }
    }
}*/
