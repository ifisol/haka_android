package haka.ifisol.haka.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Adapters.DailyMotionAdopter;
import haka.ifisol.haka.Controller.Adapters.VideosAdopter;
import haka.ifisol.haka.Controller.Adapters.YoutubAdopter;
import haka.ifisol.haka.Controller.Async.GetDailyMotionVideosTask;
import haka.ifisol.haka.Controller.Async.GetYoutubeVideosTask;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.VideoAcat;
import haka.ifisol.haka.Model.VideoSubCat;
import haka.ifisol.haka.Model.Youtube;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Activities.DownloadActivity;
import haka.ifisol.haka.View.Activities.ItemDecorationAlbumColumns;
import haka.ifisol.haka.View.Activities.YoutubPlayerActivity;

/**
 * Created by MEH on 1/11/2017.
 */
public class YoutubeVideosFragment extends Fragment implements View.OnClickListener {
    DeshboardActvty mMenueActivity;
    YoutubeVideosFragment frag;
    LayoutInflater layoutinflater;
    EditText edttextSearchVideoy;
    Context context;
    RecyclerView mRcyclerViewy;
    VideosAdopter videosAdopter;
    ArrayList<Youtube> mList;
    RecyclerViewHeader headery;
    GlobalActivity globalActivity;
    ImageView imgSearch;
    View view;
    private SwipeRefreshLayout swipeRefreshLayout;
    /*VideoAcat videoAcat=new VideoAcat();*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frag = this;
        mMenueActivity = (DeshboardActvty) getActivity();
        HashMap<String, String> param = new HashMap<>();
        param.put("part", "id,snippet");
        param.put("safeSearch", "strict");
        param.put("type", "video");
        param.put("videoType", "any");
        param.put("order", "rating");
        param.put("maxResults", "50");
        param.put("key", "AIzaSyBQYV_yimu5cOI_1FReap7TnzGcVfP4cTI");
        new GetYoutubeVideosTask(frag,mMenueActivity,param,"").execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view==null)
        {
          view = inflater.inflate(R.layout.test_youtube, container, false);

        }
        return Init(view);

    }

    private View Init(View view) {
        //mListView = (ListView) view.findViewById(R.id.list_dailymotion);
        context = getActivity();
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        imgSearch=(ImageView) view.findViewById(R.id.img_search);
        globalActivity=(GlobalActivity) context.getApplicationContext();
        edttextSearchVideoy = (EditText)view.findViewById(R.id.ed_search);
        headery = (RecyclerViewHeader)view.findViewById(R.id.header);
          mRcyclerViewy=(RecyclerView)view.findViewById(R.id.recycler_view);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(context,2);
        mRcyclerViewy.hasFixedSize();
        mRcyclerViewy.setLayoutManager(gridLayoutManager);
        mRcyclerViewy.addItemDecoration(new ItemDecorationAlbumColumns(getResources().
                getDimensionPixelSize(R.dimen.dp_1), 5));
        headery.attachTo(mRcyclerViewy);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList=new ArrayList<Youtube>();

      /*  if (mMenueActivity.mInterstitialAd.isLoaded())
        {
            mMenueActivity.mInterstitialAd.show();
        }*/
        if (mMenueActivity.mInterstitialAd.isLoaded()) {
           // mMenueActivity.mInterstitialAd.show();
        }
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!edttextSearchVideoy.getText().toString().equals(""))
                {
                    HashMap<String, String> param = new HashMap<>();
                    param.put("part", "snippet");
                    param.put("safeSearch", "strict");
                    param.put("type", "video");
                    param.put("q", edttextSearchVideoy.getText().toString());
                    param.put("order", "rating");
                    param.put("maxResults", "50");
                    param.put("key", "AIzaSyBQYV_yimu5cOI_1FReap7TnzGcVfP4cTI");
                    new GetYoutubeVideosTask(frag,mMenueActivity,param,"referesh").execute();
                }
                else
                {
                    HashMap<String, String> param = new HashMap<>();
                    param.put("part", "id,snippet");
                    param.put("safeSearch", "strict");
                    param.put("type", "video");
                    param.put("videoType", "any");
                    param.put("order", "rating");
                    param.put("maxResults", "50");
                    param.put("key", "AIzaSyBQYV_yimu5cOI_1FReap7TnzGcVfP4cTI");
                    new GetYoutubeVideosTask(frag,mMenueActivity,param,"referesh").execute();
                }
            }
        });
       /* edttextSearchVideoy.addTextChangedListener(new TextWatcher() {
            Handler handler = new Handler();
            Runnable delayedAction = null;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (delayedAction != null)
                {
                    handler.removeCallbacks(delayedAction);
                }

                //define a new search
                delayedAction = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //start your search
                        if (!edttextSearchVideoy.getText().toString().equals("")) {
                            HashMap<String, String> param = new HashMap<>();
                            param.put("part", "snippet");
                            param.put("safeSearch", "strict");
                            param.put("type", "video");
                            param.put("q", edttextSearchVideoy.getText().toString());
                            param.put("order", "rating");
                            param.put("maxResults", "50");
                            param.put("key", "AIzaSyBQYV_yimu5cOI_1FReap7TnzGcVfP4cTI");
                            new GetYoutubeVideosTask(frag,mMenueActivity,param).execute();
                        }
                        else
                        {
                            HashMap<String, String> param = new HashMap<>();
                            param.put("part", "id,snippet");
                            param.put("safeSearch", "strict");
                            param.put("type", "video");
                            param.put("videoType", "any");
                            param.put("order", "rating");
                            param.put("maxResults", "50");
                            param.put("key", "AIzaSyBQYV_yimu5cOI_1FReap7TnzGcVfP4cTI");
                            new GetYoutubeVideosTask(frag,mMenueActivity,param).execute();
                        }
                    }
                };

                //delay this new search by one second
                handler.postDelayed(delayedAction, 2000);
            }
        });*/

       /* Bundle bundl = getArguments();
        videoAcat = (VideoAcat) bundl.getSerializable("obj");*/
       // layoutinflater = getActivity().getLayoutInflater();
       // ViewGroup header = (ViewGroup) layoutinflater.inflate(R.layout.item_dailymotionlist_header, mListView, false);


       // mListView.addHeaderView(header);

     imgSearch.setOnClickListener(this);
       /* DeshboardActvty.imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DailogSubCat(videoAcat.getArrayList());
                String s="dkjjhjd";
            }
        });*/
      /* mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Youtube youtube=(Youtube)youtubAdopter.getItem(position-1);
               Bundle bundle=new Bundle();
               bundle.putSerializable("video_id",youtube);
               Utility.ReplaceFragment(new YoutubePlayerFragment(), getFragmentManager(),bundle);
           }
       });*/
    }

    /*public void DailogSubCat() {
        final ArrayList<VideoSubCat> arrayList=videoAcat.getArrayList();
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.dialog_gym_listing);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_event_nam);

        tvTitle.setText("Search Videos");

        ListView newsList = (ListView) dialog.findViewById(R.id.list_gyms);
        final YtVideosSbCatAdopt youtub=new YtVideosSbCatAdopt(context,arrayList);
        newsList.setAdapter(youtub);

        newsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                VideoSubCat videoSubCat=(VideoSubCat)arrayList.get(position);
                HashMap<String, String> param = new HashMap<>();
                param.put("part", "snippet");
                param.put("safeSearch", "strict");
                param.put("type", "video");
                param.put("q", videoSubCat.getVideo_sub_cat_name());
                param.put("order", "rating");
                param.put("maxResults", "50");
                param.put("key", "AIzaSyBQYV_yimu5cOI_1FReap7TnzGcVfP4cTI");
                new GetYoutubeVideosTask(frag,mMenueActivity,param).execute();
                dialog.dismiss();
            }
        });


        ImageView mImgBack= (ImageView)dialog.findViewById(R.id.img_bac);

        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();


    }*/

    public void API_RESULT(ArrayList<Youtube> mArray) {
        mList.clear();
        swipeRefreshLayout.setRefreshing(false);
        mList=mArray;
        if (mList!=null || mList.size()>0)
        {
            videosAdopter=new VideosAdopter(mList,context);
            mRcyclerViewy.setAdapter(videosAdopter);
        }

        videosAdopter.setOnItemClickListener(new VideosAdopter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                switch (v.getId())
                {
                    case R.id.img_play:
                        Youtube youtube=(Youtube)videosAdopter.arrayList.get(position);
                        Intent i = new Intent(context, YoutubPlayerActivity.class);
                        i.putExtra("key", youtube);
                        startActivity(i);
                       // Utility.OpenActivityNextByData(i,getActivity());
                        break;
                    case R.id.img_dnload:
                        if (globalActivity.GetDownloadingVideo()==null) {
                            int conterAddView = globalActivity.GetYtCounter();
                            if (conterAddView >= 3) {
                                if (mMenueActivity.mInterstitialAd.isLoaded()) {
                                    mMenueActivity.mInterstitialAd.show();
                                    globalActivity.SaveYoutubVideoCounter(0);
                                }
                            } else {
                                Youtube youtube2 = (Youtube)videosAdopter.arrayList.get(position);
                                String Baseurl = "https://www.youtube.com/watch?v=";
                                String viDeoId = youtube2.getVideo_Yid();
                                String URL = Baseurl + "" + viDeoId;
                                Intent ii = new Intent(getActivity(), DownloadActivity.class);
                                ii.putExtra("url", URL);
                                startActivity(ii);
                            }
                        }
                        else
                        {
                            Utility.ShowAlertDialog(getActivity(), "Alert", "A Video is already downloading you cannot download more then one video at once");
                        }

                        break;
                }


            }
        });

    }

    @Override
    public void onClick(View v) {
        if (!edttextSearchVideoy.getText().toString().equals("") & edttextSearchVideoy.getText().toString().length() > 0) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            HashMap<String, String> param = new HashMap<>();
            param.put("part", "snippet");
            param.put("safeSearch", "strict");
            param.put("type", "video");
            param.put("q", edttextSearchVideoy.getText().toString());
            param.put("order", "rating");
            param.put("maxResults", "50");
            param.put("key", "AIzaSyBQYV_yimu5cOI_1FReap7TnzGcVfP4cTI");
            new GetYoutubeVideosTask(frag, mMenueActivity, param, "").execute();
        }
    }
}
