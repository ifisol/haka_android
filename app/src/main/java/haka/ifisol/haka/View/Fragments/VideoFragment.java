/*package haka.ifisol.haka.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.R;
import ifisol.haka.Controller.Adapters.VideosAdopter;
import ifisol.haka.Controller.Async.GetVideosTask;
import ifisol.haka.Global.GlobalActivity;
import ifisol.haka.Model.News;
import ifisol.haka.Model.Videos;
import ifisol.haka.Utilities.Utility;
import ifisol.haka.View.Activities.DeshboardActvty;
import ifisol.haka.View.Activities.DownloadActivity;
import ifisol.haka.View.Activities.ItemOffsetDecoration;

*//**
 * Created by MEH on 12/22/2016.
 *//*
public class VideoFragment extends Fragment {
    GlobalActivity mGlobal;
    Context mContext;
    ArrayList<News> mArray;
    RecyclerView mRcyclerView;
    DeshboardActvty mMenueActivity;
    GridLayoutManager gridLayoutManager;
    VideosAdopter mVideoAdopter;
    VideoFragment videoFragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videos, container, false);
        mMenueActivity=(DeshboardActvty)getActivity();
        videoFragment=this;
        return Init(view);
    }

    private View Init(View view)
    {
        mContext=getActivity();
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
       // DeshboardActvty.imGFilter.setVisibility(View.GONE);
        DeshboardActvty.imgBack.setVisibility(View.VISIBLE);
        HashMap<String, String> param = new HashMap<>();
        param.put("part", "snippet");
        param.put("playlistId", "UU0vrmjhkmbCxYUGl8KNFxMA");
        param.put("key", "AIzaSyBQYV_yimu5cOI_1FReap7TnzGcVfP4cTI");

        new GetVideosTask(videoFragment,mMenueActivity,param).execute();
        mArray=new ArrayList<>();
        mRcyclerView=(RecyclerView)view.findViewById(R.id.recycler_view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        clickListener();

}

    private void clickListener() {

        DeshboardActvty.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    public void API_RESULT(final ArrayList<Videos> arrayList) {
        gridLayoutManager = new GridLayoutManager(mContext, 2);
        mRcyclerView.setHasFixedSize(true);
        mRcyclerView.setLayoutManager(gridLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.dp_1);
        mRcyclerView.addItemDecoration(itemDecoration);
        *//*mRcyclerView.addItemDecoration(new ItemDecorationAlbumColumns(getResources().
                getDimensionPixelSize(R.dimen.dp_3), 2));*//*
        *//*mVideoAdopter = new VideosAdopter(arrayList, mContext);
        mRcyclerView.setAdapter(mVideoAdopter);*//*
        mVideoAdopter.setOnItemClickListener(new VideosAdopter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                switch (v.getId())
                {
                    case R.id.img_play:
                        Videos videos=(Videos)arrayList.get(position);
                        Bundle bundle=new Bundle();
                        bundle.putSerializable("video_id",videos);
                        Utility.ReplaceFragment(new YoutubePlayerFragment(), getFragmentManager(),bundle);
                        break;
                    case R.id.tv_download:
                        Videos videos1=(Videos)arrayList.get(position);
                        String Baseurl="https://www.youtube.com/watch?v=";
                        String viDeoId=videos1.getVideo_id();
                        String URL=Baseurl+""+viDeoId;
                        Intent i = new Intent(getActivity(), DownloadActivity.class);
                        i.putExtra("url",URL);
                        startActivity(i);
                        break;
                }


            }
        });

    }
}*/
