/*
package haka.ifisol.haka.View.Fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import haka.ifisol.haka.R;
import ifisol.haka.Controller.Async.GetEventTask;
import ifisol.haka.Model.Events;
import ifisol.haka.View.Activities.DeshboardActvty;

*/
/**
 * Created by MEH on 12/22/2016.
 *//*

public class EventsFragment extends Fragment {
    Context mContext;
    WebView webView;
    Events events;
    String url;
    EventsFragment eventsFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_haka, container, false);
        return Init(view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack()) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }

                return false;
            }
        });
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            DeshboardActvty.mViewProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;

        }


        @Override
        public void onPageFinished(WebView view, String url) {
            DeshboardActvty.mViewProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }
    }

    private View Init(View view) {
        eventsFragment=this;
        DeshboardActvty.imGFilter.setVisibility(View.GONE);
        DeshboardActvty.tvHead.setText("Event");
        DeshboardActvty.imgBack.setVisibility(View.VISIBLE);
        new GetEventTask(context,eventsFragment,deshboardActvty,param).execute();
        Bundle bundl = getArguments();
        events= (Events)bundl.getSerializable("event");
        url=events.getCat_keyword();
        mContext=getActivity();
        webView=(WebView)view.findViewById(R.id.webview_haka);
        ClickListener();
        return view;

    }

    private void ClickListener() {
        DeshboardActvty.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }
}
*/
