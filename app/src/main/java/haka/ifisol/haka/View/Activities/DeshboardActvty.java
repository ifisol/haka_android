package haka.ifisol.haka.View.Activities;

import android.Manifest;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Fragments.DailymotionRecyclerfragment;
import haka.ifisol.haka.View.Fragments.DownloadFragment;
import haka.ifisol.haka.View.Fragments.EventsFilterFragment;
import haka.ifisol.haka.View.Fragments.Fragment_new_setting;
import haka.ifisol.haka.View.Fragments.NewsFilterFragment;
import haka.ifisol.haka.View.Fragments.SearchFragment;
import haka.ifisol.haka.View.Fragments.SettingsFragment;
import haka.ifisol.haka.View.Fragments.VideosMainFragment;
import haka.ifisol.haka.View.Fragments.YoutubePlayerFragment;
import haka.ifisol.haka.View.Fragments.YoutubeVideosFragment;


/**
 * Created by MEH on 1/6/2017.
 */
public class DeshboardActvty extends FragmentActivity implements View.OnClickListener {
    BottomBar bottomBar;
    Fragment fragmentInstance;
    public static TextView tvHead;
    public static ImageView imgBack;
    public static View mViewProgressBar;
    public static ImageView imgFilter;
    public InterstitialAd mInterstitialAd;
    Context mContext;
    VideosMainFragment videoFragment;
    DownloadFragment downloadFrag;


    GlobalActivity mGlobal;
    DeshboardActvty mActivity;
    private AdView mAdView;
    boolean isNeverAskAgainGallery = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deashboard);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
        tvHead = (TextView) findViewById(R.id.tv_videos);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgFilter = (ImageView) findViewById(R.id.img_filt);
        mViewProgressBar = (View) findViewById(R.id.view_progrss);
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        mContext = this;
        mActivity = this;
        mGlobal = (GlobalActivity) mContext.getApplicationContext();
        /*if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }*/
        imgBack.setVisibility(View.GONE);
        imgFilter.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });


        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(int tabId) {

              // String tag=(String)fragmentInstance.getTag();
                if (tabId == R.id.tab_video) {
                    VideosMainFragment fragmentDemo = (VideosMainFragment)getSupportFragmentManager().findFragmentByTag("SOMETAG");
                    if (fragmentDemo==null) {
                        Fragment newFragment = new VideosMainFragment();
                        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fram_Container, newFragment, "SOMETAG");
                        transaction.addToBackStack("");
                        transaction.commit();
                    }
                    else
                    {
                        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fram_Container, fragmentDemo, "SOMETAG");
                        transaction.addToBackStack("");
                        transaction.commit();
                    }

                    //fragmentInstance = getSupportFragmentManager().findFragmentByTag("video_frag");
                   // String tag=(String)fragmentInstance.getTag();
                   /* if (!(fragmentInstance instanceof VideosMainFragment))

                        Utility.ReplaceFragment(new VideosMainFragment(), getSupportFragmentManager(),"video_frag");*/
                   // if (fragmentInstance==null)
                   // {
                   //     Utility.ReplaceFragment(new VideosMainFragment(), getSupportFragmentManager(),"video_frag");
                   // }
                   // else
                   // {

                  //  }
                }
                if (tabId == R.id.tab_download) {
                    fragmentInstance = getSupportFragmentManager().findFragmentById(R.id.fram_Container);
                    if (!(fragmentInstance instanceof DownloadFragment))
                        Utility.ReplaceFragment(new DownloadFragment(), getSupportFragmentManager(), "download_frag");


                }
                if (tabId == R.id.tab_news) {
                    fragmentInstance = getSupportFragmentManager().findFragmentById(R.id.fram_Container);
                    {
                        if (!(fragmentInstance instanceof NewsFilterFragment))
                            Utility.ReplaceFragment(new NewsFilterFragment(), getSupportFragmentManager(),"new_frag");
                    }
                }
                if (tabId == R.id.tab_haka) {
                    fragmentInstance = getSupportFragmentManager().findFragmentById(R.id.fram_Container);
                    {
                        if (!(fragmentInstance instanceof EventsFilterFragment))
                            Utility.ReplaceFragment(new EventsFilterFragment(), getSupportFragmentManager(),"haka_frag");
                    }
                }
                if (tabId == R.id.tab_profile) {
                    fragmentInstance = getSupportFragmentManager().findFragmentById(R.id.fram_Container);
                    {
                        if (!(fragmentInstance instanceof SettingsFragment))
                            Utility.ReplaceFragment(new SettingsFragment(), getSupportFragmentManager(),"profile_frag");
                    }
                }

            }
        });
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {
        fragmentInstance = getSupportFragmentManager().findFragmentById(R.id.fram_Container);
        if (fragmentInstance instanceof EventsFilterFragment) {
            if (((EventsFilterFragment) fragmentInstance).webView.canGoBack()) {
                ((EventsFilterFragment) fragmentInstance).webView.goBack();
            }
        } else if (fragmentInstance instanceof NewsFilterFragment) {
            if (((NewsFilterFragment) fragmentInstance).webView.canGoBack()) {
                ((NewsFilterFragment) fragmentInstance).webView.goBack();
            }
        } else if (fragmentInstance instanceof VideosMainFragment) {
        }
        else if (fragmentInstance instanceof YoutubeVideosFragment) {
        } else if (fragmentInstance instanceof YoutubePlayerFragment) {
            fragmentInstance.getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onClick(View v) {
        fragmentInstance = getSupportFragmentManager().findFragmentById(R.id.fram_Container);
        switch (v.getId()) {
            case R.id.img_filt:
                if (fragmentInstance instanceof NewsFilterFragment)
                    ((NewsFilterFragment) fragmentInstance).showNewsListDilog();
                if (fragmentInstance instanceof EventsFilterFragment)
                    ((EventsFilterFragment) fragmentInstance).showEventListDilog();
                if (fragmentInstance instanceof DailymotionRecyclerfragment)
                    imgFilter.setVisibility(View.GONE);
                if (fragmentInstance instanceof YoutubeVideosFragment)
                    imgFilter.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {

        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();


    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

}
