package haka.ifisol.haka.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Adapters.DailyMotionAdopter;
import haka.ifisol.haka.Controller.Adapters.DailymotionRecyclerAdopter;
import haka.ifisol.haka.Controller.Adapters.VideosAdopter;
import haka.ifisol.haka.Controller.Adapters.YoutubAdopter;
import haka.ifisol.haka.Controller.Async.GetDailyMotionVideosTask;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.DailyMotion;
import haka.ifisol.haka.Model.Youtube;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Activities.DailymotionPlayerActivity;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Activities.ItemDecorationAlbumColumns;
import haka.ifisol.haka.View.Activities.ItemOffsetDecoration;
import haka.ifisol.haka.View.Activities.YoutubPlayerActivity;

/**
 * Created by MEH on 1/27/2017.
 */

public class DailymotionRecyclerfragment extends Fragment implements View.OnClickListener {
    DeshboardActvty mMenueActivity;
    DailymotionRecyclerfragment frag;
    LayoutInflater layoutinflater;
    EditText edttextSearchVideo;
    Context context;
    ListView mListView;
    DailymotionRecyclerAdopter mAdoptery;
    TextView tvSearch;
    LinearLayoutManager linearLayoutManager;
    RecyclerView mRcyclerView;
    ArrayList<DailyMotion> mList;
    RecyclerViewHeader header;
    ImageView imgSearch;
    GlobalActivity mGlobal;
    private SwipeRefreshLayout swipeRefreshLayout;
    View view;
    TextWatcher textWatcher = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMenueActivity=(DeshboardActvty)getActivity();
        frag=this;
        HashMap<String, String> param = new HashMap<>();
        param.put("fields", "id,title,thumbnail_url%2Ctitle");
        param.put("page", "1");
        param.put("limit", "100");
        new GetDailyMotionVideosTask(frag,mMenueActivity,param,"").execute();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view==null)
        {
           view = inflater.inflate(R.layout.layout_test, container, false);
        }

        return Init(view);

    }

    private View Init(View view) {

        context=getActivity();
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);

        mGlobal=(GlobalActivity) context.getApplicationContext();
        imgSearch=(ImageView) view.findViewById(R.id.img_search_dailymotion);
        //tvSearch = (TextView)view.findViewById(R.id.tv_search_dailymotion);
        header = (RecyclerViewHeader)view.findViewById(R.id.header_dailymotion);
        mRcyclerView=(RecyclerView)view.findViewById(R.id.recycler_view_dailmotion);
        edttextSearchVideo=(EditText) view.findViewById(R.id.ed_search_dailymotion);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(context,2);
        mRcyclerView.hasFixedSize();
        mRcyclerView.setLayoutManager(gridLayoutManager);
        mRcyclerView.addItemDecoration(new ItemDecorationAlbumColumns(getResources().
                getDimensionPixelSize(R.dimen.dp_1), 5));
       // ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen.dp_1);
        //mRcyclerView.addItemDecoration(itemDecoration);
        header.attachTo(mRcyclerView);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!edttextSearchVideo.getText().toString().equals(""))
                {
                    HashMap<String, String> param = new HashMap<>();
                    param.put("fields", "id,title,thumbnail_url%2Ctitle");
                    param.put("search", edttextSearchVideo.getText().toString());
                    param.put("page", "1");
                    param.put("limit", "100");
                    new GetDailyMotionVideosTask(frag, mMenueActivity, param,"referesh").execute();
                }
                else
                {
                    HashMap<String, String> param = new HashMap<>();
                    param.put("fields", "id,title,thumbnail_url%2Ctitle");
                    param.put("page", "1");
                    param.put("limit", "100");
                    new GetDailyMotionVideosTask(frag,mMenueActivity,param,"referesh").execute();
                }

            }
        });
       /* edttextSearchVideo.addTextChangedListener(new TextWatcher() {
            Handler handler = new Handler();
            Runnable delayedAction = null;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (delayedAction != null)
                {
                    handler.removeCallbacks(delayedAction);
                }

                //define a new search
                delayedAction = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        String ss=s.toString();
                        //start your search
                        if (!s.equals(""))
                        {

                        }
                        else {
                            if (!edttextSearchVideo.getText().toString().equals("")) {
                                HashMap<String, String> param = new HashMap<>();
                                param.put("fields", "id,title,thumbnail_url%2Ctitle");
                                param.put("search", edttextSearchVideo.getText().toString());
                                param.put("page", "1");
                                param.put("limit", "100");
                                new GetDailyMotionVideosTask(frag, mMenueActivity, param).execute();
                            } else {
                                HashMap<String, String> param = new HashMap<>();
                                param.put("fields", "id,title,thumbnail_url%2Ctitle");
                                param.put("page", "1");
                                param.put("limit", "100");
                                new GetDailyMotionVideosTask(frag, mMenueActivity, param).execute();
                            }
                        }
                    }
                };

                //delay this new search by one second
                handler.postDelayed(delayedAction, 3000);

            }
        });*/
        imgSearch.setOnClickListener(this);
    }

       /* Bundle bundl = getArguments();
        videoAcat = (VideoAcat) bundl.getSerializable("obj");*/



    public void API_RESULT(ArrayList<DailyMotion> arrayList) {
        swipeRefreshLayout.setRefreshing(false);
        mList=new ArrayList<DailyMotion>();
        mList.clear();
        mList=arrayList;
        mAdoptery=new DailymotionRecyclerAdopter(mList,context);
        mRcyclerView.setAdapter(mAdoptery);
        mAdoptery.setOnItemClickListener(new DailymotionRecyclerAdopter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                switch (v.getId()) {
                    case R.id.img_play_dailymotion:
                        DailyMotion dailyMotion=(DailyMotion)mList.get(position);
                      /*  Bundle bundle=new Bundle();
                        bundle.putSerializable("video_id",dailyMotion);
                        Utility.ReplaceFragment(new DailyMotionPlayerFragment(), getFragmentManager(), bundle);*/
                        Intent i = new Intent(context, DailymotionPlayerActivity.class);
                        i.putExtra("video_id", dailyMotion);
                        //Utility.OpenActivityNextByData(i,getActivity());
                        startActivity(i);
                        break;
                }
            }
        });

    }

 /*   @Override
    public void onClick(View v) {

    }*/

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (!edttextSearchVideo.getText().toString().equals("") & edttextSearchVideo.getText().toString().length() > 0) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            HashMap<String, String> param = new HashMap<>();
            param.put("fields", "id,title,thumbnail_url%2Ctitle");
            param.put("search", edttextSearchVideo.getText().toString());
            param.put("page", "1");
            param.put("limit", "100");
            new GetDailyMotionVideosTask(frag, mMenueActivity, param,"").execute();
        }
    }
}
