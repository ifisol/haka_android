package haka.ifisol.haka.View.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Adapters.VideosMainAdopter;
import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Activities.DeshboardActvty;


public class VideosMainFragment extends Fragment {
    ViewPager vPager;
    VideosMainAdopter pAdopter;
    TabLayout tabLayout;
    DeshboardActvty mMenueActivity;
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DeshboardActvty.tvHead.setText("VIDEOS");
        pAdopter=new VideosMainAdopter(getChildFragmentManager());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view==null)
        {
            view = inflater.inflate(R.layout.fragment_main_video, container, false);
        }
        return Init(view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
            mMenueActivity=(DeshboardActvty)getActivity();
        vPager.setAdapter(pAdopter);
        tabLayout.setupWithViewPager(vPager);
        vPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private View Init(View view) {
        vPager       =     (ViewPager)view.findViewById(R.id.viewPager);
        tabLayout    =     (TabLayout)view.findViewById(R.id.tab_layout);
        return  view;
    }
}
