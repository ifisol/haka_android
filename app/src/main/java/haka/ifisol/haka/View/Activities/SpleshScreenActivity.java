package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;

/**
 * Created by MEH on 2/28/2017.
 */

public class SpleshScreenActivity extends Activity {
    GlobalActivity mGlobal;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splesh);
        context=this;
        mGlobal = (GlobalActivity)context.getApplicationContext();
        final Handler handl=new Handler();
        handl.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mGlobal.GetActiveUser()==null) {
                    Utility.OpenActivityNext(context,SpleshScreenActivity.this,LoginActivity.class);
                } else
                {
                    Utility.OpenActivityNext(context,SpleshScreenActivity.this,DeshboardActvty.class);
                }
            }

        }, 3000);
    }
}
