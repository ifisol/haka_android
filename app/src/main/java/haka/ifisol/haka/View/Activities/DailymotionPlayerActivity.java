package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebView;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.DailyMotion;
import haka.ifisol.haka.Model.Youtube;
import haka.ifisol.haka.R;

/**
 * Created by MEH on 1/29/2017.
 */

public class DailymotionPlayerActivity extends Activity {
    private DMWebVideoView mVideoVieww;
    GlobalActivity mGlobal;
    Context mContext;
    String VIDEO_ID;
    DailyMotion dailyMotion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dailymotion_player);
        Intent intent = getIntent();
        dailyMotion=(DailyMotion)intent.getExtras().getSerializable("video_id");
        VIDEO_ID=dailyMotion.getVideoID();
        mVideoVieww = (DMWebVideoView) findViewById(R.id.WebVideoView);
        mVideoVieww.setVideoId(VIDEO_ID);
        mVideoVieww.setAutoPlay(true);
        mVideoVieww.load();
        mVideoVieww.play();


    }

    @Override
    protected void onPause() {
        super.onPause();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mVideoVieww.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mVideoVieww.onResume();
        }
    }

    @Override
    public void onBackPressed() {
        mVideoVieww.handleBackPress(DailymotionPlayerActivity.this);
    }
}
