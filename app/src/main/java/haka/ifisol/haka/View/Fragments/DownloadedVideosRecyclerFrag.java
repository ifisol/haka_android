package haka.ifisol.haka.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import haka.ifisol.haka.Controller.Adapters.DownloadsVideosREcyclerAdopter;
import haka.ifisol.haka.Model.DownloadedVideos;
import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Activities.DeshboardActvty;
import haka.ifisol.haka.View.Activities.ItemDecorationAlbumColumns;

/**
 * Created by MEH on 1/30/2017.
 */

public class DownloadedVideosRecyclerFrag extends Fragment implements AdapterView.OnItemClickListener {
    String[] fileList = null;
    ListView list;
    RecyclerView recyclerView;
    Context context;
    String FILE_PATH = "Haka";
    String MiME_TYPE = "video/mp4";
    DownloadsVideosREcyclerAdopter downloadsVideosREcyclerAdopter;
    ArrayList<String> mArray;
    DownloadedVideosRecyclerFrag downloadFragment;
    ArrayList<DownloadedVideos>mArrayVideos;
    int pos;
    String whichfrag;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_download_recycler, container, false);
         pos= getArguments().getInt("POSITION");
        return Init(view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mArrayVideos=new ArrayList<DownloadedVideos>();
        downloadFragment=this;
        // mArray=new ArrayList<String>();
        DeshboardActvty.imgFilter.setVisibility(View.GONE);
        DeshboardActvty.imgBack.setVisibility(View.GONE);
        updateSongList();
       /* list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
               *//* showDialogDeletVideo(position);*//*
                return true;
            }
        });*/

        if (mArrayVideos != null) {
            downloadsVideosREcyclerAdopter=new DownloadsVideosREcyclerAdopter(context,mArrayVideos,downloadFragment,whichfrag);
            recyclerView.setAdapter(downloadsVideosREcyclerAdopter);
            downloadsVideosREcyclerAdopter.setOnItemClickListener(new DownloadsVideosREcyclerAdopter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    DownloadedVideos downloadedVideos=(DownloadedVideos)mArrayVideos.get(position);
                    File videoFiles = new File(Environment.getExternalStorageDirectory().toString()+"/Haka");
                    String url=videoFiles+""+"/"+downloadedVideos.getVideoName();
                    if (!(url ==null)||!url.equals("")) {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                        File newFile = new File(url);
                        intent.setDataAndType(Uri.fromFile(newFile), MiME_TYPE);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "There is no file", Toast.LENGTH_SHORT).show();

                    }
                }
            });
            /*list.setAdapter(new HakaVideosAdopter(context, fileList));*/
        }
     /*   ListViewClickListener();*/
    }

  /*  private void showDialogDeletVideo(final int position) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_delete_video);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        final EditText edt_mail=(EditText)dialog.findViewById(R.id.edt_eemail);
        Button mBttonok = (Button) dialog.findViewById(R.id.bt_ok);
        Button mBttonCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        mBttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        mBttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File videoFiles = new File(Environment.getExternalStorageDirectory().toString()+"/Haka");
                String url=videoFiles+""+"/"+downloadsVideosREcyclerAdopter.getItem(position);
                File file = new File(url);
                if (file.exists()) {
                    context.deleteFile(url);
                }

                downloadsVideosREcyclerAdopter.RemoveItem(position);
                Toast.makeText(getActivity(), "Video deleted", Toast.LENGTH_SHORT).show();
                dialog.dismiss();

            }
        });
        dialog.show();

    }*/

  /*  private void ListViewClickListener() {
        list.setOnItemClickListener(this);

    }*/

    private void updateSongList() {

        File videoFiles = new File(Environment.getExternalStorageDirectory().toString()+"/Haka");
        if (videoFiles.isDirectory()) {
            fileList = videoFiles.list();
            DownloadedVideos downloadedVideos;
            mArray= new ArrayList<>(Arrays.<String>asList(fileList));
            File videoFile = new File(Environment.getExternalStorageDirectory().toString()+"/Haka");
            for (int i=0;i<mArray.size();i++)
            {
                String url=videoFiles+""+"/"+mArray.get(i);
                downloadedVideos=new DownloadedVideos();
                downloadedVideos.setVideoName(mArray.get(i));
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(context, Uri.parse(url));
                String time1 = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DATE);
                String title=  retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(url, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                downloadedVideos.setVideoDate(time1);
                downloadedVideos.setVideoThunmbNail(bMap);
                mArrayVideos.add(downloadedVideos);

            }


        }
        if (mArray == null) {
            Toast.makeText(getActivity(), "There is no file", Toast.LENGTH_SHORT).show();
        }

    }

    private View Init(View view) {
        context=getActivity();
        //list = (ListView)view.findViewById(R.id.lv_videoz);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_view_down);
        if (pos==0)
        {
            GridLayoutManager gridLayoutManager=new GridLayoutManager(context,2);
            recyclerView.hasFixedSize();
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.addItemDecoration(new ItemDecorationAlbumColumns(getResources().
                    getDimensionPixelSize(R.dimen.dp_1), 5));
            whichfrag="grid";
        }
        else if (pos==1)
        {
            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
            recyclerView.setLayoutManager(linearLayoutManager);

            whichfrag="linear";
        }

        //DeshboardActvty.imGFilter.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        File videoFiles = new File(Environment.getExternalStorageDirectory().toString()+"/Haka");
        String url=videoFiles+""+"/"+mArray.get(position);
        if (!(url ==null)||!url.equals("")) {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            File newFile = new File(url);
            intent.setDataAndType(Uri.fromFile(newFile), MiME_TYPE);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(getActivity(), "There is no file", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onDestroy() {
        Thread.currentThread().interrupt();
        super.onDestroy();
    }
}
