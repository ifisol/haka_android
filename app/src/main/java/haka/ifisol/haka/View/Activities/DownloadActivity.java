package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.DownloadingVideo;
import haka.ifisol.haka.R;
import haka.ifisol.haka.YoutubeExtractor.YouTubeUriExtractor;
import haka.ifisol.haka.YoutubeExtractor.YtFile;

public class DownloadActivity extends Activity {

    private static final int ITAG_FOR_AUDIO = 140;

    private static String youtubeLink;

    private long deownloadRefernce;

    private LinearLayout mainLayout;
    private ProgressBar mainProgressBar;
    GlobalActivity globalActivity;
    Context context;
    private List<YtFragmentedVideo> formatsToShowList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        globalActivity=(GlobalActivity)context.getApplicationContext();
        setContentView(R.layout.activity_sample_download);
        mainLayout = (LinearLayout) findViewById(R.id.main_layout);
        mainProgressBar = (ProgressBar) findViewById(R.id.prgrBar);

        //Check how it was started and if we can get the youtube link
//        if  (savedInstanceState == null && Intent.ACTION_SEND.equals(getIntent().getAction())
//                && getIntent().getType() != null && "text/plain".equals(getIntent().getType())) {

            String ytLink = getIntent().getStringExtra("url");

            if (ytLink != null
                    && (ytLink.contains("://youtu.be/") || ytLink.contains("youtube.com/watch?v="))) {
                youtubeLink = ytLink;
                // We have a valid link
                getYoutubeDownloadUrl(youtubeLink);
            } else {
                Toast.makeText(this, R.string.error_no_yt_link, Toast.LENGTH_LONG).show();
                finish();
            }
//        } else if (savedInstanceState != null && youtubeLink != null) {
//            getYoutubeDownloadUrl(youtubeLink);
//        } else {
//            finish();
//
//        }
    }


    private void getYoutubeDownloadUrl(String youtubeLink) {
        YouTubeUriExtractor ytEx = new YouTubeUriExtractor(this) {

            @Override
            public void onUrisAvailable(String videoId, String videoTitle, SparseArray<YtFile> ytFiles) {
                mainProgressBar.setVisibility(View.GONE);
                if (ytFiles == null) {
                    TextView tv = new TextView(DownloadActivity.this);
                    tv.setText(R.string.app_update);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    mainLayout.addView(tv);
                    return;
                }
                formatsToShowList = new ArrayList<>();
                for (int i = 0, itag; i < ytFiles.size(); i++) {
                    itag = ytFiles.keyAt(i);
                    YtFile ytFile = ytFiles.get(itag);

                    if (ytFile.getMeta().getHeight() == -1 || ytFile.getMeta().getHeight() >= 360) {
                        addFormatToList(ytFile, ytFiles);
                    }
                }
                Collections.sort(formatsToShowList, new Comparator<YtFragmentedVideo>() {
                    @Override
                    public int compare(YtFragmentedVideo lhs, YtFragmentedVideo rhs) {
                        return lhs.height - rhs.height;
                    }
                });
                for (YtFragmentedVideo files : formatsToShowList) {
                    addButtonToMainLayout(videoTitle, files);
                }
            }
        };
        ytEx.setIncludeWebM(false);
        ytEx.setParseDashManifest(true);
        ytEx.execute(youtubeLink);

    }

    private void addFormatToList(YtFile ytFile, SparseArray<YtFile> ytFiles) {
        int height = ytFile.getMeta().getHeight();
        if (height != -1) {
            for (YtFragmentedVideo frVideo : formatsToShowList) {
                if (frVideo.height == height && (frVideo.videoFile == null ||
                        frVideo.videoFile.getMeta().getFps() == ytFile.getMeta().getFps())) {
                    return;
                }
            }
        }
        YtFragmentedVideo frVideo = new YtFragmentedVideo();
        frVideo.height = height;
        if (ytFile.getMeta().isDashContainer()) {
            if (height > 0) {
                frVideo.videoFile = ytFile;
                frVideo.audioFile = ytFiles.get(ITAG_FOR_AUDIO);
            } else {
                frVideo.audioFile = ytFile;
            }
        } else {
            frVideo.videoFile = ytFile;
        }
        formatsToShowList.add(frVideo);
    }


    private void addButtonToMainLayout(final String videoTitle, final YtFragmentedVideo ytFrVideo) {
        // Display some buttons and let the user choose the format
        String btnText;

         if (ytFrVideo.height == -1)
            btnText = "Audio " + ytFrVideo.audioFile.getMeta().getAudioBitrate() + " kbit/s";
        else

        if (!(ytFrVideo.height == -1)) {
            btnText = (ytFrVideo.videoFile.getMeta().getFps() == 60) ? ytFrVideo.height + "p60" :
                    ytFrVideo.height + "p";
            Button btn = new Button(this);
            btn.setTextColor(context.getResources().getColor(R.color.white));
           // btn.setBackgroundColor(context.getResources().getColor(R.color.haka_them));
            btn.setText(btnText);
            if (ytFrVideo.videoFile.getMeta().getFps() == 60)
                btn.setVisibility(View.GONE);
            btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    String filename;
                    if (videoTitle.length() > 55) {
                        filename = videoTitle.substring(0, 55);
                    } else {
                        filename = videoTitle;
                    }
                    filename = filename.replaceAll("\\\\|>|<|\"|\\||\\*|\\?|%|:|#|/", "");
                    filename += (ytFrVideo.height == -1) ? "" : "-" + ytFrVideo.height + "p";
                    String downloadIds = "";
                    boolean hideAudioDownloadNotification = false;
                    if (ytFrVideo.videoFile != null) {
                        downloadIds += downloadFromUrl(ytFrVideo.videoFile.getUrl(), videoTitle,
                                filename + "." + ytFrVideo.videoFile.getMeta().getExt(), false);
                        downloadIds += "-";
                        hideAudioDownloadNotification = true;
                    }
                    if (ytFrVideo.audioFile != null) {
                        downloadIds += downloadFromUrl(ytFrVideo.audioFile.getUrl(), videoTitle,
                                filename + "." + ytFrVideo.audioFile.getMeta().getExt(), hideAudioDownloadNotification);
                    }
                    if (ytFrVideo.audioFile != null)
                        cacheDownloadIds(downloadIds);
                    finish();
                }
            });
            mainLayout.addView(btn);
        }
    }

    private long downloadFromUrl(String youtubeDlUrl, String downloadTitle, String fileName, boolean hide) {
        Uri uri = Uri.parse(youtubeDlUrl);
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Haka/");
        if (!file.exists()) {
            file.mkdirs();
        }
        File video = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Haka/"+fileName);
        if (video.exists()) {
            Toast.makeText(getApplicationContext(), "File already exist", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getApplicationContext(), "Start Downloading...", Toast.LENGTH_LONG).show();
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setTitle(downloadTitle);
            if (hide) {
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
                request.setVisibleInDownloadsUi(false);
            } else
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            request.setDestinationInExternalPublicDir("/Haka", fileName);

            final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            deownloadRefernce = manager.enqueue(request);
            DownloadingVideo downloadingVideo=new DownloadingVideo();
            downloadingVideo.setVideoName(fileName);
            downloadingVideo.setVideoID(deownloadRefernce);
            globalActivity.SaveDownloadingVideo(downloadingVideo);
        }
        return deownloadRefernce/*manager.enqueue(request)*/;
    }

    private void cacheDownloadIds(String downloadIds) {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Haka/");
        file.mkdirs();
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class YtFragmentedVideo {
        int height;
        YtFile audioFile;
        YtFile videoFile;
    }


}
