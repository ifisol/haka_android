package haka.ifisol.haka.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Adapters.EventFilterAdopter;
import haka.ifisol.haka.Controller.Async.GetEventTask;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.Events;
import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Activities.DeshboardActvty;

/**
 * Created by MEH on 1/2/2017.
 */
public class EventsFilterFragment extends Fragment {
    public WebView webView;
    Context context;
    ListView listView;
    Fragment fragmentInstance;
    EventsFilterFragment mFrag;
    DeshboardActvty deshboardActvty;
    GlobalActivity mGlobal;
    ArrayList<Events>mArray;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_filter, container, false);
        return Init(view);

    }

    private View Init(View view) {
        context=getActivity();
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        progressBar.setMax(100);
        webView = (WebView)view.findViewById(R.id.webView);
        mGlobal=(GlobalActivity)context.getApplicationContext();
        deshboardActvty=(DeshboardActvty)getActivity();
        DeshboardActvty.tvHead.setText("EVENTS");
        mFrag=this;
        String user_ID=mGlobal.GetActiveUser().getUser_id();
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", user_ID);
        new GetEventTask(context,mFrag,deshboardActvty,param).execute();
        DeshboardActvty.imgBack.setVisibility(View.GONE);
        DeshboardActvty.imgFilter.setVisibility(View.VISIBLE);
        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack()) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }

                return false;
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       /* DeshboardActvty.imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentInstance = getFragmentManager().findFragmentById(R.id.fram_Container);
                if (fragmentInstance instanceof EventsFilterFragment)
                {
                    showEventListDilog();
                }

            }


        });*/
    }

    public void API_RESULT(ArrayList<Events> array) {

        /*mAdopter=new EventFilterAdopter(context,array);
        listView.setAdapter(mAdopter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Events events=(Events)mAdopter.getItem(position);
                Bundle bundle=new Bundle();
                bundle.putSerializable("event",events);
                Utility.ReplaceFragment(new EventsFragment() , getFragmentManager(),bundle);
            }
        });*/
        mArray=array;
        Events events = (Events) mArray.get(0);
        String url = events.getCat_keyword();
        loadWebview(url);
    }



    private void loadWebview(String url) {
        webView.setWebViewClient(new WebViewClientDemo());
        webView.setWebChromeClient(new WebChromeClientDemo());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
    private class WebViewClientDemo extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            progressBar.setProgress(100);
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);
        }
    }
    private class WebChromeClientDemo extends WebChromeClient {
        public void onProgressChanged(WebView view, int progress) {
            progressBar.setProgress(progress);
        }
    }
    public void showEventListDilog() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.dialog_gym_listing);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_event_nam);

        tvTitle.setText("EVENTS");

        ListView eventList = (ListView) dialog.findViewById(R.id.list_gyms);
        final EventFilterAdopter mAdopter = new EventFilterAdopter(context,mArray);
        eventList.setAdapter(mAdopter);

        eventList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Events events = (Events)mAdopter.getItem(position);
                String url = events.getCat_keyword();
                loadWebview(url);
                dialog.dismiss();
            }
        });


        ImageView mImgBack= (ImageView)dialog.findViewById(R.id.img_bac);

        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();



    }
    }


