package haka.ifisol.haka.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import haka.ifisol.haka.R;
import haka.ifisol.haka.View.Activities.DeshboardActvty;

/**
 * Created by MEH on 12/22/2016.
 */
public class NewsMainFragment extends Fragment{
    ListView lvNews;
    Context mContext;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_newa, container, false);
        return Init(view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private View Init(View view) {
        mContext=getActivity();
        DeshboardActvty.tvHead.setText("News");
        return  view;
    }
}
