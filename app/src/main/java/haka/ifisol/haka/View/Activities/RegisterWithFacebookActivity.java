package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Async.LoginWithFacebookTask;
import haka.ifisol.haka.Controller.Async.SignUpwithFacebookTask;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;

/**
 * Created by MEH on 1/6/2017.
 */
public class RegisterWithFacebookActivity extends Activity implements View.OnClickListener {
    ImageView imgProfile;
    EditText edtUserName;
    EditText edtUserEmail;
    EditText edtUserPassword;
    EditText edtUserConfirmPassword;
    View viewSucess;
    Context context;
    LoginManager loginManager;
    private CallbackManager callbackManager;

    String profileimgURL;
    String email;
    String name;
    Button bttRegister;
    public static View mViewProgressBar;
    String classTHIS;


    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_registeration);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        init();
        classTHIS = getIntent().getExtras().getString("value");
        loginManager = LoginManager.getInstance();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        progress=new ProgressDialog(RegisterWithFacebookActivity.this);
        progress.setMessage("please wait");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        loginManager.logInWithReadPermissions(RegisterWithFacebookActivity.this, Arrays.asList("public_profile,user_friends,email"));


        callbackManager = CallbackManager.Factory.create();
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
               // Toast.makeText(RegisterWithFacebookActivity.this, "success", Toast.LENGTH_SHORT).show();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                setProfileToView(object);
                                Log.v("Main", response.toString());
                                if(loginResult.getAccessToken() != null){
                                    LoginManager.getInstance().logOut();
                                }
                                //Toast.makeText(RegisterWithFacebookActivity.this, "complet", Toast.LENGTH_SHORT).show();

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,picture.width(150).height(150)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Utility.OpenActivityPrevious(context, RegisterWithFacebookActivity.this, LoginActivity.class);
                //Toast.makeText(RegisterWithFacebookActivity.this, "Registeration cancel", Toast.LENGTH_SHORT).show();
                progress.dismiss();
            }

            @Override
            public void onError(FacebookException error) {
                Utility.OpenActivityPrevious(context, RegisterWithFacebookActivity.this, LoginActivity.class);
                Toast.makeText(RegisterWithFacebookActivity.this, "There is an error please try again later", Toast.LENGTH_SHORT).show();
                progress.dismiss();
            }
        });

    }

    private void setProfileToView(JSONObject object) {

        if (!(object ==null)) {
            try {
                if (classTHIS.equals("register")) {
                    email = object.getString("email");
                    name = object.getString("name");
                    profileimgURL = object.getJSONObject("picture").getJSONObject("data").getString("url");
                    viewSucess.setVisibility(View.VISIBLE);
                    edtUserName.setText(name);
                    edtUserEmail.setText(email);
                    edtUserName.setEnabled(false);
                    edtUserEmail.setEnabled(false);
                    if (profileimgURL != null && profileimgURL.length() > 0) {
                        Picasso.with(context)
                                .load(profileimgURL)
                                .error(R.drawable.capture)
                                .into(imgProfile);
                    }
                    loginManager.logOut();
                }
                else if (classTHIS.equalsIgnoreCase("login"))
                {
                    viewSucess.setVisibility(View.GONE);
                    email = object.getString("email");
                    name = object.getString("name");
                    profileimgURL = object.getJSONObject("picture").getJSONObject("data").getString("url");
                    HashMap<String, String> param = new HashMap<>();
                    param.put("email", email.toString());
                   // param.put("photo", profileimgURL);
                    param.put("facebook", "yes");
                    param.put("device_type", "Android");
                    param.put("device_token", "");
                    GlobalActivity.SaveFacebookUserImagURL(profileimgURL);
                    new LoginWithFacebookTask(RegisterWithFacebookActivity.this, param).execute();
                }

                }catch(JSONException e){
                    Utility.OpenActivityPrevious(context, RegisterWithFacebookActivity.this, LoginActivity.class);
                    Toast.makeText(RegisterWithFacebookActivity.this, "There is an error please try again later", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

       }
        else
        {
            Toast.makeText(RegisterWithFacebookActivity.this, "There is an error please try again later", Toast.LENGTH_SHORT).show();
            Utility.OpenActivityPrevious(context, RegisterWithFacebookActivity.this, LoginActivity.class);

        }
    }

    private void init() {
         context=this;
         mViewProgressBar = (View) findViewById(R.id.view_progrss_fbragister);
         imgProfile=(ImageView)findViewById(R.id.img_profile_facebook_regstr);
         edtUserName=(EditText)findViewById(R.id.edt_user_name_facebk);
         edtUserEmail=(EditText)findViewById(R.id.edt_user_email_facebk);
         viewSucess=(View)findViewById(R.id.view_success);
         bttRegister=(Button)findViewById(R.id.bt_register_user_fb);
         bttRegister.setOnClickListener(this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        loginManager.logOut();

    }

    @Override
    public void onClick(View v) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_name", edtUserName.getText().toString());
        param.put("email", edtUserEmail.getText().toString());
        param.put("facebook", "yes");
        param.put("device_type", "Android");
        param.put("device_token", "");
        new SignUpwithFacebookTask(RegisterWithFacebookActivity.this, param).execute();
    }

    public void API_RESULT() {

        Utility.OpenActivityNext(context, RegisterWithFacebookActivity.this, DeshboardActvty.class);

    }

    public void API_RESULT_LOGIN() {
        Utility.OpenActivityNext(context, RegisterWithFacebookActivity.this, DeshboardActvty.class);
    }

    public void LOGIN_RESULT() {
        finish();
    }
}

