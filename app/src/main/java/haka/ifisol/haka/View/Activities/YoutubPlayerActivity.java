package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import haka.ifisol.haka.Model.Youtube;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Fragments.YoutubePlayerFragment;


/**
 * Created by MEH on 1/19/2017.
 */

public class YoutubPlayerActivity extends FragmentActivity{
    Context mContext;
    Youtube youtube;
    Configuration configuration;
    Boolean isScrean=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtubplayer);
        mContext=this;

        Intent intent = getIntent();
        youtube=(Youtube) intent.getExtras().getSerializable("key");
        Bundle bundle=new Bundle();
        bundle.putSerializable("video_id",youtube);
        Utility.ReplaceFragmentYoutub(new YoutubePlayerFragment(), getSupportFragmentManager(),bundle);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
           // Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            isScrean=false;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
           // Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            isScrean=true;
        }
    }

    @Override
    public void onBackPressed() {
        if (!isScrean)
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
        else
        {
            finish();
        }
    }
}
