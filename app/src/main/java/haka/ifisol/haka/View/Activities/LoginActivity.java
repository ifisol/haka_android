package haka.ifisol.haka.View.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import haka.ifisol.haka.Controller.Async.ForgotPasswordTask;
import haka.ifisol.haka.Controller.Async.LoginTask;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    //Button but_register;
    View viewRegister;
    Button but_login;
    Context mContext;
    EditText Memail;
    EditText Mpassword;
    public static View mViewProgressBar;
    LoginActivity mActivity;
    TextView tvForgotPass;
    View viewFcbook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_redesign);
        Init();
        ClickListener();
    }

    private void ClickListener() {
        viewRegister.setOnClickListener(this);
        but_login.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);
        viewFcbook.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.view_register:
                Intent i = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(i);
                break;
            case R.id.bt_login:
                //Utility.OpenActivityNext(mContext, LoginActivity.this,DeshboardActvty.class);
                loginTask();
                break;
            case R.id.tv_forgot_password:
                ShowDialog();
                break;
            case R.id.view_facbook:
                Intent intent = new Intent(this,RegisterWithFacebookActivity.class);
                intent.putExtra("value", "login");
                Utility.OpenActivityNextByData(intent, mActivity);

                break;

        }

    }

    private void ShowDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_forgot_password);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        final EditText edt_mail=(EditText)dialog.findViewById(R.id.edt_eemail);
        Button mBttonok = (Button) dialog.findViewById(R.id.bt_ok);
        Button mBttonCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        mBttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        mBttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mail=edt_mail.getText().toString();
                if (mail.isEmpty()) {
                    Utility.ShowAlertDialog(mActivity, "User Email", "Please enter a valid email");
                }
                else {
                    if (Utility.EmailValidator(mail))
                    {
                        HashMap<String, String> param = new HashMap<>();
                        param.put("email", mail);
                        new ForgotPasswordTask(LoginActivity.this,param).execute();
                        dialog.dismiss();
                    }
                    else
                    {
                        Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
                    }
                }
            }
        });
        dialog.show();
    }

    private void loginTask() {
        boolean isValid = true;

        if (Memail.getText().toString().isEmpty()) {
            if (isValid) {
                Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
                isValid = false;
            }
        }
        if (!Utility.EmailValidator(Memail.getText().toString())) {
            if (isValid) {
                Utility.ShowAlertDialog(mActivity, "Email", "Please enter a valid email");
                isValid = false;
            }
        }
        if (Mpassword.getText().toString().isEmpty()) {
            if (isValid)
                Utility.ShowAlertDialog(mActivity, "Password", "Please enter password");
            isValid = false;
        }
        if (Mpassword.getText().toString().length()<6){
            if (isValid) {
                Utility.ShowAlertDialog(mActivity, "Password", "Password should contain at least 6 character");
                isValid = false;
            }
        }
        if (isValid) {

            HashMap<String, String> param = new HashMap<>();
            param.put("email", Memail.getText().toString());
            param.put("password", Mpassword.getText().toString());
            param.put("device_type", "Android");
            param.put("facebook", "no");
            param.put("device_token", "");

            new LoginTask(LoginActivity.this, param).execute();
        }

    }

    private void Init() {
        mContext=this;
        mActivity=this;
        viewFcbook=(View)findViewById(R.id.view_facbook);
        mViewProgressBar = (View) findViewById(R.id.view_progrss_login);
        //but_register=(Button)findViewById(R.id.bt_register);
        but_login=(Button)findViewById(R.id.bt_login);
        viewRegister=(View) findViewById(R.id.view_register);
        Memail=(EditText)findViewById(R.id.edit_email);
        Memail.setText("mehranrajpoot329@gmail.com");
        Mpassword=(EditText)findViewById(R.id.edit_pass);
        Mpassword.setText("123456");
        tvForgotPass=(TextView)findViewById(R.id.tv_forgot_password);

    }
    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        }


    }

    public void API_RESULT() {
        Utility.OpenActivityNext(mContext, LoginActivity.this,DeshboardActvty.class);
    }
}
