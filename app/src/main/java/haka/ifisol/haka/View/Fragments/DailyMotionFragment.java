/*package haka.ifisol.haka.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Adapters.DailyMotionAdopter;
import haka.ifisol.haka.Controller.Async.GetDailyMotionVideosTask;
import haka.ifisol.haka.Model.DailyMotion;
import haka.ifisol.haka.Model.VideoAcat;
import haka.ifisol.haka.Model.VideoSubCat;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Activities.DeshboardActvty;


*//**
 * Created by MEH on 1/10/2017.
 *//*
public class DailyMotionFragment extends Fragment implements View.OnClickListener {
    DailyMotionAdopter mAdopter;
    ListView mListView;
    DeshboardActvty mMenueActivity;
    DailyMotionFragment frag;
    LayoutInflater layoutinflater;
    EditText edttextSearchVideo;
    Context context;
    TextView tvSearch;
    //RecyclerView recyclerView;
    ArrayList<DailyMotion>mList;
    *//*VideoAcat videoAcat=new VideoAcat();*//*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dailymotion, container, false);
        return Init(view);

    }

    private View Init(View view) {
        mListView=(ListView)view.findViewById(R.id.list_dailymotion);
        mMenueActivity=(DeshboardActvty)getActivity();
        //recyclerView=(RecyclerView) view.findViewById(R.id.recycler_view);
        context=getActivity();
        frag=this;
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       *//* Bundle bundl = getArguments();
        videoAcat = (VideoAcat) bundl.getSerializable("obj");*//*
        layoutinflater = getActivity().getLayoutInflater();

        ViewGroup header = (ViewGroup)layoutinflater.inflate(R.layout.item_dailymotionlist_header,mListView,false);
        edttextSearchVideo=(EditText)header.findViewById(R.id.ed_search);
        tvSearch=(TextView)header.findViewById(R.id.tv_search);
        mListView.addHeaderView(header);
        HashMap<String, String> param = new HashMap<>();
        param.put("fields", "id,title,thumbnail_url%2Ctitle");
        param.put("page", "1");
        param.put("limit", "30");
        new GetDailyMotionVideosTask(frag,mMenueActivity,param).execute();
        tvSearch.setOnClickListener(this);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DailyMotion dailyMotion=(DailyMotion)mAdopter.getItem(position-1);
                Bundle bundle=new Bundle();
                bundle.putSerializable("video_id",dailyMotion);
                Utility.ReplaceFragment(new DailyMotionPlayerFragment(), getFragmentManager(), bundle);
            }
        });
      *//*  DeshboardActvty.imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogVideoSubCat();
            }
        });*//*

    }

   *//* public void DialogVideoSubCat() {
        final ArrayList<VideoSubCat> arrayList=videoAcat.getArrayList();
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.dialog_gym_listing);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_event_nam);

        tvTitle.setText("Search Videos");

        ListView newsList = (ListView) dialog.findViewById(R.id.list_gyms);
        final YtVideosSbCatAdopt youtub=new YtVideosSbCatAdopt(context,arrayList);
        newsList.setAdapter(youtub);

        newsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                VideoSubCat videoSubCat=(VideoSubCat)arrayList.get(position);
                HashMap<String, String> param = new HashMap<>();
                param.put("fields", "id,title,thumbnail_url%2Ctitle");
                param.put("search", videoSubCat.getVideo_sub_cat_name());
                param.put("page", "1");
                param.put("limit", "30");
                new GetDailyMotionVideosTask(frag,mMenueActivity,param).execute();
                dialog.dismiss();
            }
        });


        ImageView mImgBack= (ImageView)dialog.findViewById(R.id.img_bac);

        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }*//*


    public void API_RESULT(ArrayList<DailyMotion> arrayList) {
       mList=arrayList;
        mAdopter=new DailyMotionAdopter(mList,context);
        mListView.setAdapter(mAdopter);
        edttextSearchVideo.setText("");
    }

    @Override
    public void onClick(View v) {
        if (!edttextSearchVideo.getText().toString().equals("")&edttextSearchVideo.getText().toString().length()>0)
        {
            HashMap<String, String> param = new HashMap<>();
            param.put("fields", "id,title,thumbnail_url%2Ctitle");
            param.put("search", edttextSearchVideo.getText().toString());
            param.put("page", "1");
            param.put("limit", "30");
            new GetDailyMotionVideosTask(frag,mMenueActivity,param).execute();
        }
    }
}*/
