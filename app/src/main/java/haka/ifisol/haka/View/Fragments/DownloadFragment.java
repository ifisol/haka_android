package haka.ifisol.haka.View.Fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpicker.ImagePickerActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import haka.ifisol.haka.Controller.Adapters.HakaVideosAdopter;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.DownloadedVideos;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Activities.DeshboardActvty;

/**
 * Created by MEH on 12/22/2016.
 */
public class DownloadFragment extends Fragment implements AdapterView.OnItemClickListener {
    String[] fileList = null;
    ListView list;
    Context context;
    String FILE_PATH = "Haka";
    String MiME_TYPE = "video/mp4";
    HakaVideosAdopter hakaVideoAdopter;
    ArrayList<String> mArray;
    DownloadFragment downloadFragment;
    GlobalActivity mGlobal;
    DeshboardActvty activity;
    boolean isNeverAskAgainGallery = false;
    ArrayList<DownloadedVideos> mArrayVideos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_download, container, false);
        return Init(view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        activity = (DeshboardActvty) getActivity();
        mGlobal = (GlobalActivity) context.getApplicationContext();
        mArrayVideos = new ArrayList<DownloadedVideos>();
        downloadFragment = this;
        DeshboardActvty.tvHead.setText("DOWNLOADS");
        DeshboardActvty.imgFilter.setVisibility(View.GONE);
        DeshboardActvty.imgBack.setVisibility(View.GONE);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            UPDATEVIDEOSLIST();
            ListViewClickListener();
        }
        else {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                downloadFragment.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
            } else {
                UPDATEVIDEOSLIST();
                ListViewClickListener();
            }
        }
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showDialogDeletVideo(position);
                return true;
            }
        });


    }

    private void UPDATEVIDEOSLIST() {
        updateSongList();

        if (mArray != null) {
            if (mGlobal.GetDownloadingVideo()==null) {
                hakaVideoAdopter = new HakaVideosAdopter(context, mArrayVideos, downloadFragment);
                list.setAdapter(hakaVideoAdopter);

            }
            else {
              /*  for (int i = 0; i < mArrayVideos.size(); i++)
                {

                    if (mGlobal.GetDownloadingVideo().getVideoName().equals(mArrayVideos.get(i).getVideoName()))
                    {
                        mArrayVideos.add(0, mArrayVideos.get(i));

                    }
                }*/
                hakaVideoAdopter = new HakaVideosAdopter(context, mArrayVideos, downloadFragment);

                list.setAdapter(hakaVideoAdopter);

            }
        }
    }

    private void ShowPermissionWarning(final String s, String s1, String s2) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_permission_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvHeading = (TextView) dialog.findViewById(R.id.tv_heading);
        TextView tvDesc = (TextView) dialog.findViewById(R.id.tv_desc);
        TextView tvRetry = (TextView) dialog.findViewById(R.id.tv_retry);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        tvHeading.setText(s2);
        tvDesc.setText(s1);
        tvRetry.setText(s);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (s.equalsIgnoreCase("retry")) {
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showDialogDeletVideo(final int position) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_delete_video);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) ((double) dm.heightPixels * (.40));
        window.setAttributes(lp);
        final EditText edt_mail = (EditText) dialog.findViewById(R.id.edt_eemail);
        Button mBttonok = (Button) dialog.findViewById(R.id.bt_ok);
        Button mBttonCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        mBttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        mBttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File videoFiles = new File(Environment.getExternalStorageDirectory().toString() + "/Haka");
                DownloadedVideos videoName = mArrayVideos.get(position);
                String url = videoFiles + "" + "/" + videoName.getVideoName();
                if (!(mGlobal.GetDownloadingVideo() == null) && mGlobal.GetDownloadingVideo().getVideoName().equals(videoName.getVideoName())) {
                    Utility.ShowAlertDialog(getActivity(), "Alert", "You can't delete this video while video is downloading");
                    dialog.dismiss();
                } else {
                    File file = new File(url);
                    if (file.exists()) {
                        //context.deleteFile(url);
                        try {
                            file.getCanonicalFile().delete();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }


                    hakaVideoAdopter.RemoveItem(position);
                    Toast.makeText(getActivity(), "Video deleted", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }


            }
        });
        dialog.show();

    }

    private void ListViewClickListener() {
        list.setOnItemClickListener(this);

    }

    private void updateSongList() {

        File videoFiles = new File(Environment.getExternalStorageDirectory().toString() + "/Haka");
        if (videoFiles.isDirectory()) {
            fileList = videoFiles.list();
            DownloadedVideos downloadedVideos;
            mArray = new ArrayList<>(Arrays.<String>asList(fileList));
            File videoFile = new File(Environment.getExternalStorageDirectory().toString() + "/Haka");
            if (mArray != null) {
                for (int i = 0; i < mArray.size(); i++) {
                    String url = videoFiles + "" + "/" + mArray.get(i);
                    downloadedVideos = new DownloadedVideos();
                    downloadedVideos.setVideoName(mArray.get(i));
                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    retriever.setDataSource(context, Uri.parse(url));
                    String time1 = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DATE);
                    String title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                    Bitmap bMap = ThumbnailUtils.createVideoThumbnail(url, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                    downloadedVideos.setVideoDate(time1);
                    downloadedVideos.setVideoThunmbNail(bMap);
                    mArrayVideos.add(downloadedVideos);

                }
            }


        }
        if (mArray == null) {
            Toast.makeText(getActivity(), "There is no file", Toast.LENGTH_SHORT).show();
        }

    }

    private View Init(View view) {
        list = (ListView) view.findViewById(R.id.lv_videoz);
        //DeshboardActvty.imGFilter.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        File videoFiles = new File(Environment.getExternalStorageDirectory().toString() + "/Haka");
        DownloadedVideos videoName = (DownloadedVideos) hakaVideoAdopter.getItem(position);
        String url = videoFiles + "" + "/" + videoName.getVideoName();
        if (!(url == null) || !url.equals("")) {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            File newFile = new File(url);
            intent.setDataAndType(Uri.fromFile(newFile), MiME_TYPE);
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "There is no file", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onDestroy() {
        Thread.currentThread().interrupt();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 5:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    UPDATEVIDEOSLIST();

                } else {
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[0]) || !ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[0])) {
                            ShowPermissionWarning("Go to settings", "You selected 'Never ask again' but Storage permission is compulsory for Kivi Haka. You can still allow permission from settings. Go to settings and select permissions", "Storage Permission Required");
                        }
                }
                break;
        }

    }

    @Override
    public void onResume() {
       /* if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            downloadFragment.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
        } else {
            UPDATEVIDEOSLIST();
            ListViewClickListener();
        }*/
        super.onResume();
    }
}