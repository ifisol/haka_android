package haka.ifisol.haka.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import haka.ifisol.haka.Controller.Adapters.SearchAdopter;
import haka.ifisol.haka.Controller.Async.GetVideosCatTask;
import haka.ifisol.haka.Global.GlobalActivity;
import haka.ifisol.haka.Model.VideoAcat;
import haka.ifisol.haka.R;
import haka.ifisol.haka.Utilities.Utility;
import haka.ifisol.haka.View.Activities.DeshboardActvty;

/**
 * Created by MEH on 12/22/2016.
 */
public class SearchFragment extends Fragment  {
    ListView listView;
    SearchAdopter mSearchadopter;
    Context mContext;
    GlobalActivity mGlobal;
    DeshboardActvty deshboardActvty;
    SearchFragment searchFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        return Init(view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchFragment=this;
        deshboardActvty=(DeshboardActvty)getActivity();
        DeshboardActvty.imgBack.setVisibility(View.GONE);
        DeshboardActvty.tvHead.setText("Search");
        mGlobal=(GlobalActivity)mContext.getApplicationContext();
        String user_ID=mGlobal.GetActiveUser().getUser_id();
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", user_ID);
        new GetVideosCatTask(mContext,searchFragment,deshboardActvty,param).execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                VideoAcat videoAcat=(VideoAcat)mSearchadopter.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable("obj",videoAcat);
                if (videoAcat.getVideo_cat_name().equalsIgnoreCase("Youtube"))
                {
                    Utility.ReplaceFragment(new YoutubeVideosFragment(), getFragmentManager(),bundle);
                }
                if (videoAcat.getVideo_cat_name().equalsIgnoreCase("Daily motion"))
                {
                    Utility.ReplaceFragment(new DailymotionRecyclerfragment(), getFragmentManager(),bundle);
                }

            }
        });
    }

    private View Init(View view) {
        listView=(ListView)view.findViewById(R.id.list_view);
        DeshboardActvty.imgFilter.setVisibility(View.GONE);
        mContext=getActivity();
        return view;
    }


    public void API_RESULT(ArrayList<VideoAcat> mArray) {
        mSearchadopter=new SearchAdopter(mContext,mArray);
        listView.setAdapter(mSearchadopter);
    }
}
