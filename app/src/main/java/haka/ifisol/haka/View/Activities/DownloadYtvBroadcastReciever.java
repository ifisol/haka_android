package haka.ifisol.haka.View.Activities;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.Toast;

import haka.ifisol.haka.Global.GlobalActivity;

/**
 * Created by MEH on 1/20/2017.
 */

public class DownloadYtvBroadcastReciever extends BroadcastReceiver {
    GlobalActivity globalActivity;
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        globalActivity=(GlobalActivity)context.getApplicationContext();
        if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0));
            DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            Cursor cursor = manager.query(query);
            if (cursor.moveToFirst()) {
                if (cursor.getCount() > 0) {
                    int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
                        String file = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                        long refernceid=intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID,-1);
                        globalActivity.SaveYoutubVideoCounter(globalActivity.GetYtCounter()+1);

                    }  if (status == DownloadManager.STATUS_FAILED){
                        Toast.makeText(context, "Download failed", Toast.LENGTH_SHORT).show();
                        globalActivity.SaveDownloadingVideo(null);
                        // So something here on failed.
                    }
                    else if (status == DownloadManager.ERROR_FILE_ERROR){
                         Toast.makeText(context, "Something error in file", Toast.LENGTH_SHORT).show();
                        globalActivity.SaveDownloadingVideo(null);
                        // So something here on failed.
                    }
                    else if (status == DownloadManager.ERROR_INSUFFICIENT_SPACE){
                        Toast.makeText(context, "Insufficient Space", Toast.LENGTH_SHORT).show();
                        globalActivity.SaveDownloadingVideo(null);
                        // So something here on failed.
                    }
                    else if (status == DownloadManager.STATUS_PAUSED){
                        Toast.makeText(context, "pased", Toast.LENGTH_SHORT).show();
                        globalActivity.SaveDownloadingVideo(null);
                        // So something here on failed.
                    }
                }
            }
        }
        if (action.equals(DownloadManager.ERROR_INSUFFICIENT_SPACE)) {
            Toast.makeText(context, "Insufficient Space", Toast.LENGTH_SHORT).show();
            globalActivity.SaveDownloadingVideo(null);
        }

          }
}
