package haka.ifisol.haka.View.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import haka.ifisol.haka.R;
import io.fabric.sdk.android.Fabric;

/**
 * Created by MEH on 3/27/2017.
 */
public class ShareReferalCodeTwitterActvitiy extends Activity{
    private static final String TWITTER_KEY = "uylfhULG8ExaGDK9fPNhA8jv1";
    private static final String TWITTER_SECRET = "8b8H6bVXZqpyXxsgMfHvET5sFRSi219I6aKHVQAC8hsD8Br2An";
    private TwitterLoginButton twitterButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_code_facebook_activity);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        Intent i = getIntent();
        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text("Training at my other gym with Fit Exchange @fitexchangegyms #fitexchange");
        builder.show();
        setUpViews();
    }

    private void setUpViews() {
        setUpTwitterButton();
    }

    private void setUpTwitterButton() {
        twitterButton = (TwitterLoginButton) findViewById(R.id.twitter_button);
        twitterButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Toast.makeText(ShareReferalCodeTwitterActvitiy.this, "You shared post successfully", Toast.LENGTH_SHORT).show();
                Log.d("rht", "Invitation Sent Successfully");
                //setUpViewsForTweetComposer();

            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getApplicationContext(), "Post Cancel", Toast.LENGTH_SHORT).show();
            }
        });

    }
    /*
        private void setUpViewsForTweetComposer() {
            TweetComposer.Builder builder = new TweetComposer.Builder(this)
                    .text("Fit Exchange: Champions aren't born.They're made");
            builder.show();

        }*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       /* super.onActivityResult(requestCode, resultCode, data);*/
        twitterButton.onActivityResult(requestCode, resultCode, data);
    }
}
