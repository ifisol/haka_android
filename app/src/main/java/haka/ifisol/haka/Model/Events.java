package haka.ifisol.haka.Model;

import java.io.Serializable;

/**
 * Created by MEH on 1/2/2017.
 */
public class Events implements Serializable {
    public String cat_id;
    public String cat_name;
    public String cat_keyword;

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_keyword() {
        return cat_keyword;
    }

    public void setCat_keyword(String cat_keyword) {
        this.cat_keyword = cat_keyword;
    }
}
