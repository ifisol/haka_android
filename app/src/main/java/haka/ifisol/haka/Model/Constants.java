package haka.ifisol.haka.Model;

/**
 * Created by MEH on 7/25/2016.
 */
public class Constants {
    public static String URL = "http://haka.ifisol.com/api/";
    public static String VERIFY_USER = "verify_user";
    public static String LOGIN = "login";
    public static String SIGNUP = "register";
    public static String FORGOT_PASSWORD ="forgot_password";
    public static String YOUTUBEBaseURL = "https://www.googleapis.com/youtube/v3/search?";
    public static String CHANG_PASSWORD ="change_password";
    public static String UPDATE_Profile = "update_profile";
    public static String FIT_GYM_NAME_LIST = "fit_gym_name_list";
    public static String GRT_VIDEOS_Cat = "get_video_cat_subcats";
    public static String RESEND_CODE = "resend_verification_code";
    public static String TRAIN_NOW = "train_now";
    public static String PREVIOUS_VISIT = "get_visitors_gym";
    public static String GET_NEWS = "get_news_cat_subcats";
    public static String GET_EVENT = "get_events_cat_subcats";
    public static String DailyMotionBaseUrl = "https://api.dailymotion.com/videos?";
    public static String Get_videos = "https://www.googleapis.com/youtube/v3/playlistItems?";
    public static String Img_URL = "http://haka.ifisol.com/";
}
