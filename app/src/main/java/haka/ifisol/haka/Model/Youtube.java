package haka.ifisol.haka.Model;

import java.io.Serializable;

/**
 * Created by MEH on 1/11/2017.
 */
public class Youtube implements Serializable {
    public String video_Yid;
    public String video_Ytitle;
    public String video_Ythunbail;

    public String getVideo_Yid() {
        return video_Yid;
    }

    public void setVideo_Yid(String video_Yid) {
        this.video_Yid = video_Yid;
    }

    public String getVideo_Ytitle() {
        return video_Ytitle;
    }

    public void setVideo_Ytitle(String video_Ytitle) {
        this.video_Ytitle = video_Ytitle;
    }

    public String getVideo_Ythunbail() {
        return video_Ythunbail;
    }

    public void setVideo_Ythunbail(String video_Ythunbail) {
        this.video_Ythunbail = video_Ythunbail;
    }
}
