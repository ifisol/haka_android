package haka.ifisol.haka.Model;

/**
 * Created by MEH on 12/20/2016.
 */
public class News {
    public String news_id;
    public String news_name;
    public String news_keyword;

    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getNews_name() {
        return news_name;
    }

    public void setNews_name(String news_name) {
        this.news_name = news_name;
    }

    public String getNews_keyword() {
        return news_keyword;
    }

    public void setNews_keyword(String news_keyword) {
        this.news_keyword = news_keyword;
    }
}
