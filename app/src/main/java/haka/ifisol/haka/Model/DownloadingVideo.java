package haka.ifisol.haka.Model;

/**
 * Created by MEH on 1/24/2017.
 */

public class DownloadingVideo {
    public String videoName;
    public long videoID;

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public long getVideoID() {
        return videoID;
    }

    public void setVideoID(long videoID) {
        this.videoID = videoID;
    }
}
