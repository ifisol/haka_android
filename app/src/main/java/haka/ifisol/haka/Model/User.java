package haka.ifisol.haka.Model;

/**
 * Created by MEH on 1/3/2017.
 */
public class User {
    public String facebookUser;
    public String user_id;
    public String user_name;
    public String user_email;
    public String user_password;
    public String user_registeration_date;
    public String user_last_login_date;
    public String picture;

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getFacebookUser() {
        return facebookUser;
    }

    public void setFacebookUser(String facebookUser) {
        this.facebookUser = facebookUser;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_registeration_date() {
        return user_registeration_date;
    }

    public void setUser_registeration_date(String user_registeration_date) {
        this.user_registeration_date = user_registeration_date;
    }

    public String getUser_last_login_date() {
        return user_last_login_date;
    }

    public void setUser_last_login_date(String user_last_login_date) {
        this.user_last_login_date = user_last_login_date;
    }
}
