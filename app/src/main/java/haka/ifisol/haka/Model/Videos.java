package haka.ifisol.haka.Model;

import java.io.Serializable;

/**
 * Created by MEH on 12/27/2016.
 */
public class Videos implements Serializable {

    public String video_title;

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public String getVideo_title() {
        return video_title;
    }

    public void setVideo_title(String video_title) {
        this.video_title = video_title;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String video_thumbnail;
    public String video_id;
}
