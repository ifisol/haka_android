package haka.ifisol.haka.Model;

import android.graphics.Bitmap;

/**
 * Created by MEH on 1/25/2017.
 */

public class DownloadedVideos {
    public String videoName;
    public Bitmap videoThunmbNail;
    public String videoDate;
    public int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isDownloading() {
        return isDownloading;
    }

    public void setDownloading(boolean downloading) {
        isDownloading = downloading;
    }

    public boolean isDownloading;

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public Bitmap getVideoThunmbNail() {
        return videoThunmbNail;
    }

    public void setVideoThunmbNail(Bitmap videoThunmbNail) {
        this.videoThunmbNail = videoThunmbNail;
    }

    public String getVideoDate() {
        return videoDate;
    }

    public void setVideoDate(String videoDate) {
        this.videoDate = videoDate;
    }
}
