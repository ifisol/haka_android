package haka.ifisol.haka.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by MEH on 1/13/2017.
 */

public class VideoAcat implements Serializable {
    public String video_cat_id;
    public String video_cat_name;
    public String video_Cat_keywords;
    public String video_image;

    public String getVideo_image() {
        return video_image;
    }

    public void setVideo_image(String video_image) {
        this.video_image = video_image;
    }

    public ArrayList<VideoSubCat>arrayList=new ArrayList<VideoSubCat>();

    public String getVideo_cat_id() {
        return video_cat_id;
    }

    public void setVideo_cat_id(String video_cat_id) {
        this.video_cat_id = video_cat_id;
    }

    public String getVideo_cat_name() {
        return video_cat_name;
    }

    public void setVideo_cat_name(String video_cat_name) {
        this.video_cat_name = video_cat_name;
    }

    public String getVideo_Cat_keywords() {
        return video_Cat_keywords;
    }

    public void setVideo_Cat_keywords(String video_Cat_keywords) {
        this.video_Cat_keywords = video_Cat_keywords;
    }

    public ArrayList<VideoSubCat> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<VideoSubCat> arrayList) {
        this.arrayList = arrayList;
    }
}
