package haka.ifisol.haka.Model;

import java.io.Serializable;

/**
 * Created by MEH on 1/13/2017.
 */

public class VideoSubCat implements Serializable {
    public String video_sub_cat_id;
    public String video_sub_cat_name;
    public String video_sub_cat_keyword;

    public String getVideo_sub_cat_id() {
        return video_sub_cat_id;
    }

    public void setVideo_sub_cat_id(String video_sub_cat_id) {
        this.video_sub_cat_id = video_sub_cat_id;
    }

    public String getVideo_sub_cat_name() {
        return video_sub_cat_name;
    }

    public void setVideo_sub_cat_name(String video_sub_cat_name) {
        this.video_sub_cat_name = video_sub_cat_name;
    }

    public String getVideo_sub_cat_keyword() {
        return video_sub_cat_keyword;
    }

    public void setVideo_sub_cat_keyword(String video_sub_cat_keyword) {
        this.video_sub_cat_keyword = video_sub_cat_keyword;
    }
}
