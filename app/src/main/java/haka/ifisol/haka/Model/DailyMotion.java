package haka.ifisol.haka.Model;

import java.io.Serializable;

/**
 * Created by MEH on 1/10/2017.
 */
public class DailyMotion implements Serializable {
    public String has_More;
    public String videoID;
    public String videoURL;
    public String videoTitle;

    public String getHas_More() {
        return has_More;
    }

    public void setHas_More(String has_More) {
        this.has_More = has_More;
    }

    public String getVideoID() {
        return videoID;
    }

    public void setVideoID(String videoID) {
        this.videoID = videoID;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }
}
