package haka.ifisol.haka.Global;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import haka.ifisol.haka.Model.DownloadingVideo;
import haka.ifisol.haka.Model.User;
import io.fabric.sdk.android.Fabric;


/**
 * Created by MEH on 12/20/2016.
 */
public class GlobalActivity extends Application{

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "5XHc6OcC6glVA0THDSi0ldNt4";
    private static final String TWITTER_SECRET = "rxHBkOyUsJsteqBo4H9TJO0mF2ojRpIshF4WAxUiIqPSoojxh3";

    static SharedPreferences mPreference;
    static SharedPreferences.Editor mEditor;
    String dailymotionSearchText;
    String youtubeSearchText;
    Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        context=this;
        mPreference = getSharedPreferences("MY_PREF", Context.MODE_PRIVATE);
        mEditor = mPreference.edit();
        if (GetYtCounter()==-1)
        {
            SaveYoutubVideoCounter(0);
        }


    }
    public void SaveActiveUser(User user) {

        Gson gson=new Gson();
        String mUser=gson.toJson(user,User.class);
        mEditor.putString("ACTIVE_USER",mUser);
        mEditor.commit();


    }



    public User GetActiveUser() {

        Gson gson=new Gson();
        String user=mPreference.getString("ACTIVE_USER", null);
        User mUser=gson.fromJson(user, User.class);
        return mUser;
    }
    public void SaveYoutubVideoCounter(int i) {

        mEditor.putInt("KEY",i);
        mEditor.commit();

    }
    public int GetYtCounter() {

        int savedValue = mPreference.getInt("KEY", -1);
        return savedValue;
    }
    public void SaveDownloadingVideo(DownloadingVideo downloadingVideo) {

        Gson gson=new Gson();
        String video=gson.toJson(downloadingVideo,DownloadingVideo.class);
        mEditor.putString("USER_VIDEO",video);
        mEditor.commit();
    }

    public DownloadingVideo GetDownloadingVideo() {

        Gson gson=new Gson();
        String video=mPreference.getString("USER_VIDEO", null);
        DownloadingVideo mUser=gson.fromJson(video,DownloadingVideo.class);
        return mUser;
    }
     public static void SaveFacebookUserImagURL(String url)
     {
         mEditor.putString("url",url);
         mEditor.commit();
     }
    public static String GetFacebookUserImagURL()
    {
        String savedValue = mPreference.getString("url","");
        return savedValue;
    }

}
